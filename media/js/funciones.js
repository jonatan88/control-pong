$(document).ready(function(){	

    $('.fixed-action-btn').floatingActionButton();

	$('.modal').modal();

	$('select').formSelect();
	$('.tablas').DataTable( {
		"language":{
			"decimal":        "",
			"emptyTable":     "Sin datos",
			"info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"infoEmpty":      "Mostrando 0 a 0 de 0 registros",
			"infoFiltered":   "( _MAX_ registros filtrados)",
			"infoPostFix":    "",
			"thousands":      ",",
			"lengthMenu":     " ",
			"loadingRecords": "Cargando...",
			"processing":     "Procesando...",
			"search":         "Buscar:",
			"zeroRecords":    "No se han encontrado registros.",
			"paginate": {
				"first":      "Primero",
				"last":       "Último",
				"next":       "Siguiente",
				"previous":   "Anterior"
			},
			"aria": {
				"sortAscending":  ": activate to sort column ascending",
				"sortDescending": ": activate to sort column descending"
			}
		}
	});

	

	//testing
	$('.presentacion').hide();
	$('.instrucciones_prueba').hide();
	$('.cuestionarios_panel').show();
		//ir_a_seccion('seccion_5_numero_1');
	//testing
	var pageURL = window.location.href;
	var lastURLSegment = pageURL.substr(pageURL.lastIndexOf('/') + 1);
	console.log(lastURLSegment);
	switch (lastURLSegment) {
		case "Home": 
		$(".menu_inicio").attr("style","text-decoration: underline #0085b0;"); 
		break; 
		case "Universidad": 
		$(".menu_universidad").attr("style","text-decoration: underline #0085b0;"); 
		break; 
		case "Carreras": 
		$(".menu_carrera").attr("style","text-decoration: underline #0085b0;"); 
		break; 
		case "Prueba_intereses": 
		$(".prueba_intereses").attr("style","text-decoration: underline #0085b0;"); 
		break; 
		case "Resultados": 
		$(".resultados").attr("style","text-decoration: underline #0085b0;"); 
		break; 
		case "Metas": 
		$(".metas").attr("style","text-decoration: underline #0085b0;"); 
		break; 
		default: 
	} 
	$('#correo').focusout(function() { 
		correo = $('#correo').val();
		jQuery.ajax({
			type: "POST",
			url: base_url()+"index.php/Inicio/comprobarExistenciaCorreo",
			dataType: 'json', 
			data: {correo: correo},
			success: function(res) {						  
				if (res)
				{
					console.log(res); 
					$.each(res, function(i, itm){
						console.log(itm['correo']); 
					}); 
					correo_registro_valido = 0;
					console.log(correo_registro_valido); 
					$('#result').text("El correo ya esta registrado, intenta con otro."); 					
				} else {
					correo_registro_valido = 1;				
					console.log(correo_registro_valido); 
				}			 
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				console.log('Error');
			}
		});						
	});
	
	$('#codigo').keyup(function() { 
		codigo = $('#codigo').val();
		jQuery.ajax({
			type: "POST",
			url: base_url()+"index.php/Inicio/comprobarExistencia",
			dataType: 'json', 
			data: {codigo: codigo},
			success: function(res) {						  
				if (res)
				{
					console.log(res); 
					$.each(res, function(i, itm){
						console.log(itm['idCodigos']); 
						console.log(itm['utilizado']); 
						id_codigo_actual = (itm['idCodigos']); 
						utilizado = (itm['utilizado']);  
					});
					$('#id_codigo_actual').val(id_codigo_actual);
					if(utilizado == 0){
						$('#codigo_valido').val(1);
						$('.label_codigo').text("Excelente, tu código es válido.");
					}else if(utilizado == 1){
						$('#codigo_valido').val(0);
						$('.label_codigo').text("Código ya utilizado.");
					}		
				} else {
					$('#codigo_valido').val(0);
					$('.label_codigo').text("Código no válido"); 					
				}			 
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				console.log('Error');
			}
		});						
	});
	$('.boton_inicio_sesion').on('click', function (e) {
		e.preventDefault();
		var correo_login = $("#correo_login").val();
		var password_login = $("#password_login").val();
		
		
		$.trim(correo_login);
		$.trim(password_login); 
		if(correo_login   == "" || password_login   == ""){
			if(correo_login   == ""){
				alert("Debes completar el correo.");	
			}
			if(password_login   == ""){
				alert("Debes completar la contraseña.");	
			}
		}else{	
			$.ajax({
				url: base_url()+"index.php/Login/login_validation",
				type: "POST",
				dataType: "json",
				data: {correo_login:correo_login, password_login:password_login},
				success: function(res) {
					if (res['error']==true)
					{
						console.log(res['error']); 
						$('#result_login2').text("Datos incorrectos, intenta nuevamente.");
					} else {
						console.log(res['error']); 
						$(location).attr('href', base_url()+"index.php/Home"); 
					}		
				}
			});
		}		
	});
	
	
	$('.boton_recuperar_enviar').on('click', function (e) {
		e.preventDefault();
		var email_recuperar = $("#email_recuperar").val();
		console.log(email_recuperar);
		
		$.trim(email_recuperar);
		if(email_recuperar   == ""  ){
			if(email_recuperar   == ""){
				alert("Debes completar el correo.");	
			} 
		}else{	
			$.ajax({
				url: base_url()+"index.php/Login/recuperarPassword",
				type: "POST",
				dataType: "json",
				data: {email_recuperar:email_recuperar},
				success: function(res) {
					if (res['error'] == true)
					{
						console.log(res['error']); 
						$('#result_login3').text("Datos incorrectos, intenta nuevamente.");
					} else {
						$('#result_login3').html('<span style="color: green;">En un momento se enviará un correo con tu password.</span>');
						setTimeout(function(){  
											//$(location).attr('href', base_url()); 
										}, 3000);
					}		
				}
			});
		}		
	});
	$('.boton_registro').on('click', function () {
		$('.pantalla_recuperar').hide();
		$('.pantalla_login').hide();
		$('.fondo1').hide();
		$('.pantalla_registro').show();
		$('.mensaje_mal').hide();
		$('.boton_login').attr('style','filter: brightness(1);');
		$(this).attr('style','filter: brightness(1.15);');
	}); 
	$('.boton_login').on('click', function () {
		$('.pantalla_recuperar').hide();
		$('.pantalla_registro').hide();
		$('.fondo1').hide();
		$('.pantalla_login').show();
		$('.mensaje_mal').hide();
		$('.boton_registro').attr('style','filter: brightness(1);');
		$(this).attr('style','filter: brightness(1.15);');
	}); 
	$('.boton_recuperar').on('click', function () {
		$('.pantalla_login').hide();
		$('.pantalla_registro').hide();
		$('.fondo1').hide();
		$('.pantalla_recuperar').show();
	}); 
	

	$( "#correo" ).focusout(function() {
		validarCorreo();
	});
	$( "#password_validar,#password" ).focusout(function() {
		validarContrasena();
	});




		//ready fin
		
	});
function validateEmail(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}
function validarCorreo() {
	var $result = $("#result");
	var email = $("#correo").val();
	$result.text("");
	if (validateEmail(email)) {
		$result.text("Es válido");
		$result.css("color", "green");
	} else {
		$result.text("No es válido");
		$result.css("color", "red");
	}
	return false;
}
function validarContrasena() {
	var $result = $("#result_password");
	var password = $("#password").val();
	var password_validar = $("#password_validar").val();
	$result.text("");
	if (password == password_validar) {
		$result.text("Es válida");
		$result.css("color", "green");
		password_es_valido = 1;
	} else {
		$result.text("No es la misma");
		$result.css("color", "red");
		password_es_valido = 0;
	}
	return false;
}
function validarCorreoLogin() {
	var $result = $("#result_login");
	var email = $("#correo_login").val();
	$result.text("");
	if (validateEmail(email)) {
		$result.text("Es válido");
		$result.css("color", "green");
	} else {
		$result.text("No es válido");
		$result.css("color", "red");
	}
	return false;
}
function validarCorreoRecuperar() {
	var $result = $("#result_login3");
	var email = $("#password_recuperar").val();
	$result.text("");
	if (validateEmail(email)) {
		$result.text("Es válido");
		$result.css("color", "green");
	} else {
		$result.text("No es válido");
		$result.css("color", "red");
	}
	return false;
}
function validate(evt) {
	var theEvent = evt || window.event;
		  // Handle paste
		  if (theEvent.type === 'paste') {
		  	key = event.clipboardData.getData('text/plain');
		  } else {
		  // Handle key press
		  var key = theEvent.keyCode || theEvent.which;
		  key = String.fromCharCode(key);
		}
		var regex = /[0-9]|\./;
		if( !regex.test(key) ) {
			theEvent.returnValue = false;
			if(theEvent.preventDefault) theEvent.preventDefault();
		}
	}
base_url() 
	function base_url() {
		//url = "http://pong-avance.com/control/";
		//url = "http://localhost/control/";
		var getUrl = window.location;
		var url = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1] + "/" + getUrl.pathname.split('/')[2] + "/";
 	 	//console.log(url);
		return url;
	}
	
