<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resultados extends CI_Controller { 
	public function __construct(){
		parent::__construct();            
		$this->load->helper(array('html', 'url', 'date'));
		$this->load->library(array('form_validation')); 
		$this->load->model('Model_1');
		$this->load->library('session');
		$this->load->library('email');


		
	}
	
	public function mostrarVistas($vista_nombre, $data) {		 
		$data['assets'] = $this->load->view('assets',NULL,TRUE);
		$data['menu'] = $this->load->view('menu',$data,TRUE);
		$data['footer'] = $this->load->view('footer',NULL,TRUE);

		$data['fila_carreras'] = $this->Model_1->traer_carreras($this->session->userdata('idUsuarios'));  
		$data['fila_universidades'] = $this->Model_1->traer_universidades($this->session->userdata('idUsuarios'));  
		$data['fila_cuestionario_intereses'] = $this->Model_1->traer_cuestionario_intereses($this->session->userdata('idUsuarios'));  
		$data['fila_cuestionario_aptitudes'] = $this->Model_1->traer_cuestionario_aptitudes($this->session->userdata('idUsuarios'));  
		$data['fila_cuestionario_valores'] = $this->Model_1->traer_cuestionario_valores($this->session->userdata('idUsuarios'));   
		$data['fila_cuestionario_personalidad'] = $this->Model_1->traer_cuestionario_personalidad($this->session->userdata('idUsuarios'));   
		$data['fila_cuestionario_habilidad'] = $this->Model_1->traer_cuestionario_habilidad($this->session->userdata('idUsuarios'));  

		$this->load->view($vista_nombre, $data);  
	}
	
	public function index()
	{ 
		$this->load->library('session');
		
		if($this->session->userdata('is_logged_in')) {			
			$data['nombre_usuario'] = $this->session->userdata('nombre'); 
			$data['correo_usuario'] = $this->session->userdata('correo'); 
			$this->mostrarVistas('resultados_view',$data);			
		}
		else{
			redirect('Inicio');
		} 
	} 
	
	public function Guardar_carreras()
	{ 
		$data = array(
			'carrera_1'=>$this->input->post('carrera_1'),	
			'carrera_2'=>$this->input->post('carrera_2'),	
			'carrera_3'=>$this->input->post('carrera_3'),				
			'carreras_resultados_desglose'=>$this->input->post('carreras_resultados_desglose'),				
			'tp'=> date("d-m-Y H:i:s"),				
			'usuarios_idUsuarios'=>  $this->session->userdata('idUsuarios') 				 
		);		 
		$data = $this->Model_1->insertar_carreras($data);					
		echo json_encode($data);	 		 
	}

	public function Guardar_universidades()
	{ 
		$data = array(
			'universidad_1'=>$this->input->post('universidad_1'),	
			'universidad_2'=>$this->input->post('universidad_2'),
			'universidades_resultados_desglose'=>$this->input->post('universidades_resultados_desglose'),
			'tp'=> date("d-m-Y H:i:s"),		
			'usuarios_idUsuarios'=>  $this->session->userdata('idUsuarios') 				 
		);		 
		$data = $this->Model_1->insertar_universidades($data);					
		echo json_encode($data);	 		 
	}

	public function Guardar_intereses()
	{ 
		$data = array(
			'total_intereses_alto_1'=>$this->input->post('total_intereses_alto_1'),	
			'interes_mayor_1'=>$this->input->post('interes_mayor_1'),	
			'caracteristica_mayor_1'=>$this->input->post('caracteristica_mayor_1'),	
			'total_intereses_alto_2'=>$this->input->post('total_intereses_alto_2'),	
			'interes_mayor_2'=>$this->input->post('interes_mayor_2'),	
			'caracteristica_mayor_2'=>$this->input->post('caracteristica_mayor_2'),	
			'total_intereses_alto_3'=>$this->input->post('total_intereses_alto_3'),	
			'interes_mayor_3'=>$this->input->post('interes_mayor_3'),	
			'caracteristica_mayor_3'=>$this->input->post('caracteristica_mayor_3'),	
			'total_intereses_bajo_1'=>$this->input->post('total_intereses_bajo_1'),	
			'interes_menor_1'=>$this->input->post('interes_menor_1'),	
			'caracteristica_menor_1'=>$this->input->post('caracteristica_menor_1'),
			'usuarios_idUsuarios'=>  $this->session->userdata('idUsuarios') 				 
		);		 
		$data = $this->Model_1->insertar_intereses($data);					
		echo json_encode($data);	 		 
	} 

	public function Guardar_aptitudes()
	{ 
		$data = array(
			'total_aptitudes_alto_1'=>$this->input->post('total_aptitudes_alto_1'),	
			'aptitud_mayor_1'=>$this->input->post('aptitud_mayor_1'),	
			'aptitud_caracteristica_mayor_1'=>$this->input->post('aptitud_caracteristica_mayor_1'),	
			'total_aptitudes_alto_2'=>$this->input->post('total_aptitudes_alto_2'),	
			'aptitud_mayor_2'=>$this->input->post('aptitud_mayor_2'),	
			'aptitud_caracteristica_mayor_2'=>$this->input->post('aptitud_caracteristica_mayor_2'),	
			'total_aptitudes_alto_3'=>$this->input->post('total_aptitudes_alto_3'),	
			'aptitud_mayor_3'=>$this->input->post('aptitud_mayor_3'),	
			'aptitud_caracteristica_mayor_3'=>$this->input->post('aptitud_caracteristica_mayor_3'),	
			'total_aptitudes_bajo_1'=>$this->input->post('total_aptitudes_bajo_1'),	
			'aptitudes_menor_1'=>$this->input->post('aptitudes_menor_1'),	
			'aptitudes_caracteristica_menor_1'=>$this->input->post('aptitudes_caracteristica_menor_1'),
			'usuarios_idUsuarios'=>  $this->session->userdata('idUsuarios') 				 
		);		 
		$data = $this->Model_1->insertar_aptitudes($data);					
		echo json_encode($data);	 		 
	} 
	public function Guardar_valores()
	{ 
		$data = array(
			'total_valores_alto_1'=>$this->input->post('total_valores_alto_1'),
			'valor_mayor_1'=>$this->input->post('valor_mayor_1'),
			'valor_caracteristica_mayor_1'=>$this->input->post('valor_caracteristica_mayor_1'),
			'valor_meta_mayor_1'=>$this->input->post('valor_meta_mayor_1'),
			'total_valores_alto_2'=>$this->input->post('total_valores_alto_2'),
			'valor_mayor_2'=>$this->input->post('valor_mayor_2'),
			'valor_caracteristica_mayor_2'=>$this->input->post('valor_caracteristica_mayor_2'),
			'valor_meta_mayor_2'=>$this->input->post('valor_meta_mayor_2'),
			'total_valores_alto_3'=>$this->input->post('total_valores_alto_3'),
			'valor_mayor_3'=>$this->input->post('valor_mayor_3'),
			'valor_caracteristica_mayor_3'=>$this->input->post('valor_caracteristica_mayor_3'),
			'valor_meta_mayor_3'=>$this->input->post('valor_meta_mayor_3'),
			'total_valores_bajo_1'=>$this->input->post('total_valores_bajo_1'),
			'valores_menor_1'=>$this->input->post('valores_menor_1'),
			'valores_caracteristica_menor_1'=>$this->input->post('valores_caracteristica_menor_1'),
			'valores_meta_menor_1'=>$this->input->post('valores_meta_menor_1'),
			'usuarios_idUsuarios'=>  $this->session->userdata('idUsuarios') 				 
		);		 
		$data = $this->Model_1->insertar_valores($data);					
		echo json_encode($data);	 		 
	} 
	
	public function Guardar_personalidad()
	{ 
		$data = array(
			'total_extrovertido'=>$this->input->post('total_extrovertido'),
			'total_introvertido'=>$this->input->post('total_introvertido'),
			'total_juicios'=>$this->input->post('total_juicios'),
			'total_percepciones'=>$this->input->post('total_percepciones'),
			'total_sensaciones'=>$this->input->post('total_sensaciones'),
			'total_intuicion'=>$this->input->post('total_intuicion'),
			'total_pensamiento'=>$this->input->post('total_pensamiento'),
			'total_sentimientos'=>$this->input->post('total_sentimientos'),
			'personalidad_resultado_1'=>$this->input->post('personalidad_resultado_1'),
			'personalidad_resultado_2'=>$this->input->post('personalidad_resultado_2'),
			'usuarios_idUsuarios'=>  $this->session->userdata('idUsuarios') 				 
		);		 
		$data = $this->Model_1->insertar_personalidad($data);					
		echo json_encode($data);	 		 
	} 

	public function Guardar_habilidades()
	{ 
		$data = array(
			'total_naturalista'=>$this->input->post('total_naturalista'),
			'total_intrapersonal'=>$this->input->post('total_intrapersonal'),
			'total_interpersonal'=>$this->input->post('total_interpersonal'),
			'total_corporal'=>$this->input->post('total_corporal'),
			'total_espacial'=>$this->input->post('total_espacial'),
			'total_logico_matematica'=>$this->input->post('total_logico_matematica'),
			'total_musical'=>$this->input->post('total_musical'),
			'total_linguistica'=>$this->input->post('total_linguistica'),
			'habilidad_resultado_1'=>$this->input->post('habilidad_resultado_1'),
			'habilidad_resultado_2'=>$this->input->post('habilidad_resultado_2'),
			'usuarios_idUsuarios'=>  $this->session->userdata('idUsuarios') 				 
		);		 
		$data = $this->Model_1->insertar_habilidad($data);					
		echo json_encode($data);	 		 
	} 

	public function Obtener_res_intereses_id()
	{     
		$data = $this->Model_1->Obtener_res_intereses_id($this->input->post('id'));					
		echo json_encode($data);
	} 

	public function Obtener_res_aptitudes_id()
	{     
		$data = $this->Model_1->Obtener_res_aptitudes_id($this->input->post('id'));					
		echo json_encode($data);
	} 

	public function Obtener_res_valores_id()
	{     
		$data = $this->Model_1->Obtener_res_valores_id($this->input->post('id'));					
		echo json_encode($data);
	} 

	public function Obtener_res_personalidad_id()
	{     
		$data = $this->Model_1->Obtener_res_personalidad_id($this->input->post('id'));					
		echo json_encode($data);
	} 
	public function Obtener_res_habilidad_id()
	{     
		$data = $this->Model_1->Obtener_res_habilidad_id($this->input->post('id'));					
		echo json_encode($data);
	} 

	public function Enviar_correo()
	{     

		$contenido_imprimir = $this->input->post('contenido_imprimir');
		$correo_envio = $this->input->post('correo_envio');

		$this->email
		->from('noreply@contenidos-pearson.com', 'PEARSON - Orientación Profesional')
		->to($correo_envio)
		->subject('PEARSON - Resultados Orientación Profesional')
		->message($contenido_imprimir)
		->set_mailtype('html');


		

		if($this->email->send()){
			echo 'success';
		}else{
			echo 'Error';
		}

	} 



	
}
