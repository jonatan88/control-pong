<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Codigos extends CI_Controller {

	function __construct(){ 
		parent::__construct();	
		
		$this->load->helper(array('html', 'url'));	
		$this->load->model('Codigos_model','tabla');

	} 
	public function mostrarVistas($vista_nombre, $data) {		 
		$data['assets'] = $this->load->view('assets',NULL,TRUE);
		$data['menu'] = $this->load->view('menu',$data,TRUE);
		$data['footer'] = $this->load->view('footer',NULL,TRUE);
		$this->load->view($vista_nombre, $data);  
	}

	public function index(){		   	 
		$this->mostrarVistas('codigos_view',null);
	}
	
	public function ajax_list()
	{
		$list = $this->tabla->get_datatables();
		$data = array();	 
		foreach ($list as $campos) {
			
			$row = array();
			$row[] = $campos->idCodigos;
			$row[] = $campos->codigo; 

			if($campos->utilizado == 1){
				$utilizado = "Si";
			}else{
				$utilizado = "No";		
			}
			
			
			$row[] = $utilizado;
			$row[] = $campos->codigos_tp;
			

			//add html for action
			//$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Editar" onclick="edit_person('.$campos->idEstudiantes.')"><i class="glyphicon glyphicon-pencil"></i> Editar</a>				  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Eliminar" onclick="delete_person('.$campos->idEstudiantes.')"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>';
			
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->tabla->count_all(),
			"recordsFiltered" => $this->tabla->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	
	public function ajax_edit($id)
	{
		$data = $this->tabla->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		
		$data = array(
			
			'codigo' => $this->input->post('codigo'),
			'utilizado' => $this->input->post('utilizado'),
		);
		$insert = $this->tabla->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
			

			'codigo' => $this->input->post('codigo'),
			'utilizado' => $this->input->post('utilizado')
			
			
		);
		$this->tabla->update(array('idCodigos' => $this->input->post('idCodigos')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->tabla->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	} 
}
