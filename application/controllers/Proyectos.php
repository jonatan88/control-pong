<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyectos extends CI_Controller {



	public function __construct(){
		parent::__construct();            
		$this->load->helper(array('html', 'url', 'date'));
		$this->load->model('Model_proyectos', 'model', TRUE);
		$this->load->library(array('form_validation'));  
		$this->load->library('session');		
	}
	
	public function mostrarVistas($vista_nombre, $data) {		 
		$data['assets'] = $this->load->view('assets',NULL,TRUE);
		$data['menu'] = $this->load->view('menu',$data,TRUE);
		$data['footer'] = $this->load->view('footer',NULL,TRUE);
		$this->load->view($vista_nombre, $data);  
	}
	
	public function index()
	{ 
		$this->load->library('session');
		if($this->session->userdata('is_logged_in')) {			
			$data['nombre_usuario'] = $this->session->userdata('nombre'); 
			$data['ListaClientes'] = $this->model->ListaClientes();
			$data['nombre_colaborador'] =  $this->session->userdata("nombre");


			$this->mostrarVistas('proyectos_view',$data);	 
		}
		else{
			redirect('Inicio');
		} 
	}

	public function enviarDatos() { 
		$data = array(
			'proyecto' => $this->input->post('proyecto'),
			'proy_estatus' => $this->input->post('proy_estatus'),
			'clientes_idClientes' => $this->input->post('clientes_idClientes')	
		);
		$data = $this->model->insertar($data);					
		echo json_encode(array("status" => true));
	} 




	public function obtener_datos()
	{
		$list = $this->model->traer_datos();
		$data = array();


		foreach ($list as $fila) {
			$row = array();
			if ($fila->proy_estatus == 1) {
				$estatus = "Activo";
			}else{
				$estatus = "Inactivo";
			}			

			$row[] = $fila->idProyectos;
			$row[] = $fila->proyecto;
			$row[] = $fila->cliente;
			$row[] = $estatus;
			$row[] = $fila->proy_tp;
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void();" title="Editar" onclick="editar(' . $fila->idProyectos . ');">  <i class="material-icons">edit</i> </a> ';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model->count_all(),
			"recordsFiltered" => $this->model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function ajax_edit($id) {
		$data = $this->model->get_by_id($id);
		echo json_encode($data);
	}

	public function actualizarDatos() {
		$data = array(
			'proyecto' => $this->input->post('proyecto'),
			'proy_estatus' => $this->input->post('proy_estatus'),
			'clientes_idClientes' => $this->input->post('clientes_idClientes')			 
		);
		$this->model->update(array('idProyectos' => $this->input->post('idProyectos')), $data);
		echo json_encode(array("status" => true));
	}

}
