<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("model_login");
		$this->load->helper(array('html', 'url','security','form'));	
		$this->load->library('session');
		$this->load->library('email');
		$this->load->library('encryption');



	}

	public function index() {
		$this->login();
	}
	
	public function login() {
		$this->load->view('login_vista');
	}
	
	
	public function login_validation() {
		
		
		$output = array('error' => false);
		
		$correo_login = $_POST['correo_login'];
		$password_login = $_POST['password_login'];
		
		$data = $this->model_login->can_log_in($correo_login, $password_login);
		
		if($data){
			
			$usuario_data = array(
				'idUsuarios'=>$data->idUsuarios,
				'nombre'=>$data->nombre,
				'correo'=>$data->correo,
				'is_logged_in'=>1
			);
			$this->session->set_userdata($usuario_data);
			
			$this->session->set_userdata('user', $data);
			$output['message'] = 'login correcto';
		}
		else{
			$output['error'] = true;
			$output['message'] = 'login incorrecto';
		}
		
		echo json_encode($output); 
		
		
	}
	public function recuperarPassword() {
		
		
		$output = array('error' => false);
		
		$email_recuperar = $_POST['email_recuperar'];
		
		$data = $this->model_login->recuperarPassword($email_recuperar);
		
		if($data){
			
			// echo $data;
			
			
			//$output['passwordDecript'] = $data;
			
			
			// correo
			$this->email->from('pearsonaccesscode@pearson.com', 'Pearson Orientación Profesional');
			$this->email->to($email_recuperar); 

			$this->email->subject('Recuperación de Contraseña');
			$this->email->message('Buen día, te agredecemos por utilizar nuestros servicios. <br> Tu contraseña es: '.$data);
			$this->email->set_mailtype('html');

			
			
			if($this->email->send())
			{
				
			} else {
				show_error($this->email->print_debugger());
			}
			
			
				//
			
			$output['message'] = 'login correcto';
			
			
		}
		else{
			$output['error'] = true;
			$output['message'] = 'login incorrecto';
		}
		
		echo json_encode($output); 
		
		
	}
	
	public function validate_credentials() {
		$this->load->model('model_login');
		if($this->model_login->can_log_in()){
			$usuario = $this->model_login->can_log_in();
			$usuario_data = array(
				'idUsuarios'=>$usuario->idUsuarios,
				'nombre'=>$usuario->nombre,
				'is_logged_in'=>1
			);
			$this->session->set_userdata($usuario_data);
			return true;
		}else{
			$this->form_validation->set_message('validate_credentials','Password incorrecto');
			return false;
		}
	}
	public function logout() {
		$this->session->sess_destroy();
		redirect('Inicio');
	}
	
	
	
	
}



?>