<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prueba_intereses extends CI_Controller { 
	public function __construct(){
		parent::__construct();            
		$this->load->helper(array('html', 'url', 'date'));
		$this->load->library(array('form_validation')); 
		$this->load->model('Model_1');

		
	}
	
	public function mostrarVistas($vista_nombre, $data) {		 
		$data['assets'] = $this->load->view('assets',NULL,TRUE);
		$data['menu'] = $this->load->view('menu',$data,TRUE);
		$data['select_option_6_view'] = $this->load->view('select_option_6_view',NULL,TRUE);
		$data['select_option_view'] = $this->load->view('select_option_view',NULL,TRUE);
		$data['numeros_view'] = $this->load->view('numeros_view',NULL,TRUE);
		$data['numeros_6_view'] = $this->load->view('numeros_6_view',NULL,TRUE);

		$data['fila_carreras'] = $this->Model_1->traer_carreras($this->session->userdata('idUsuarios'));  
		$data['fila_universidades'] = $this->Model_1->traer_universidades($this->session->userdata('idUsuarios'));  
		$data['fila_cuestionario_intereses'] = $this->Model_1->traer_cuestionario_intereses($this->session->userdata('idUsuarios'));  
		$data['fila_cuestionario_aptitudes'] = $this->Model_1->traer_cuestionario_aptitudes($this->session->userdata('idUsuarios'));  
		$data['fila_cuestionario_valores'] = $this->Model_1->traer_cuestionario_valores($this->session->userdata('idUsuarios'));   
		$data['fila_cuestionario_personalidad'] = $this->Model_1->traer_cuestionario_personalidad($this->session->userdata('idUsuarios'));   
		$data['fila_cuestionario_habilidad'] = $this->Model_1->traer_cuestionario_habilidad($this->session->userdata('idUsuarios'));  

		
		$data['intereses_view'] = $this->load->view('intereses_view',$data,TRUE);
		$data['aptitudes_view'] = $this->load->view('aptitudes_view',$data,TRUE);
		$data['valores_view'] = $this->load->view('valores_view',$data,TRUE);
		$data['personalidad_view'] = $this->load->view('personalidad_view',$data,TRUE);
		$data['habilidades_view'] = $this->load->view('habilidades_view',$data,TRUE);


		
		

		$data['footer'] = $this->load->view('footer',NULL,TRUE);
		$this->load->view($vista_nombre, $data);  
	}
	
	
	
	public function index()
	{ 
		$this->load->library('session');
		
		if($this->session->userdata('is_logged_in')) {
			
			$data['nombre_usuario'] = $this->session->userdata('nombre'); 
			$this->mostrarVistas('prueba_interes_view',$data);

			
		}
		else{
			redirect('Inicio');
		} 
	} 
	
}
