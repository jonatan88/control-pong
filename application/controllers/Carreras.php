<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carreras extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 	public function __construct(){
			parent::__construct();            
			$this->load->helper(array('html', 'url', 'date'));
			//$this->load->model('Model_1', 'model', TRUE);
			$this->load->library(array('form_validation')); 
		
	}
	
	public function mostrarVistas($vista_nombre, $data) {		 
		$data['assets'] = $this->load->view('assets',NULL,TRUE);
	    $data['menu'] = $this->load->view('menu',$data,TRUE);
		$data['footer'] = $this->load->view('footer',NULL,TRUE);
	    $this->load->view($vista_nombre, $data);  
	}
	
	
	
	
	public function index()
	{
		
		$this->load->library('session');
 
		//restrict users to go back to login if session has been set
		if($this->session->userdata('is_logged_in')) {
				 
			$data['nombre_usuario'] = $this->session->userdata('nombre');
			$this->mostrarVistas('tabla_carreras_view',$data);


		}
		else{
			redirect('Inicio');
		}
		
 
	
	}
	
	 
	
	public function comprobarExistencia(){
		
		$codigo =  $this->input->post('codigo');			 

	 	$result = $this->model->comprobarExistencia($codigo);
		 	
			
			if($result){
        $data['msg'] = 'true';
        }else{
           $data['msg'] = 'false';
        }
		
		
		echo json_encode($result);
	}
	
	
	public function enviarDatos() {
	 
			$id_codigo = $this->input->post('id_codigo');
			$correo = $this->input->post('correo'); 
			$codigo = $this->input->post('codigo'); 


		
 		 $data = array(
				'dni'=>$this->input->post('dni'),	
				'correo'=>$this->input->post('correo'),	
				'nombre'=>$this->input->post('nombre'),	
				'apellido'=>$this->input->post('apellido'),	
 				'sede'=>$this->input->post('sede'),	
 				'grado'=>$this->input->post('grado'),	
				'boleta'=>$this->input->post('boleta'),	
				'factura'=>$this->input->post('factura'),	
				'comprobante'=>$this->input->post('comprobante'),	
		);
		 
		 $data = $this->model->insertarEvaluacion($data,$id_codigo,$correo,$codigo);					
		 echo json_encode($data);
			
		 
	}
}
