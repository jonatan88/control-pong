<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

 
	public function __construct(){
		parent::__construct();            
		$this->load->helper(array('html', 'url', 'date'));
		$this->load->model('Model_clientes', 'model', TRUE);
		$this->load->library(array('form_validation')); 
		$this->load->library('encrypt');
		$this->load->library('session');

		
	}
	
	public function mostrarVistas($vista_nombre, $data) {		 
		$data['assets'] = $this->load->view('assets',NULL,TRUE);
		$data['menu'] = $this->load->view('menu',$data,TRUE);
		$data['footer'] = $this->load->view('footer',NULL,TRUE);
		$this->load->view($vista_nombre, $data);  
	}
	
	public function index()
	{

		$this->load->library('session');

		if($this->session->userdata('is_logged_in')) {
			$data['datos'] = $this->session->userdata('nombre'); 


			redirect('Home');

		}
		else{
			$data = null;
			$this->mostrarVistas('registro_view',$data);

		}
		
		

	}
	
	public function tabla_universidad()
	{

		$this->load->view('tabla_universidad_view');
	}
	
	public function comprobarExistencia(){		
		$codigo =  $this->input->post('codigo');
		$result = $this->model->comprobarExistencia($codigo);			
		if($result){
			$data['msg'] = 'true';
		}else{
			$data['msg'] = 'false';
		}	
		echo json_encode($result);
	}
	
	public function comprobarExistenciaCorreo(){		
		$correo =  $this->input->post('correo');
		$result = $this->model->comprobarExistenciaCorreo($correo);			
		if($result){
			$data['msg'] = 'true';
		}else{
			$data['msg'] = 'false';
		}	
		echo json_encode($result);
	}
	
	
	public function enviarDatos() {

		$passwordEncriptado = $this->encrypt->encode($this->input->post('password'));

		$correo = $this->input->post('correo'); 
		$codigo = $this->input->post('codigo'); 
		$id_codigo = $this->input->post('id_codigo'); 



		
		$data = array(
			'nombre'=>$this->input->post('nombre'),	
			'telefono'=>$this->input->post('telefono'),	
			'correo'=>$this->input->post('correo'),	
			'pais'=>$this->input->post('pais'),	
			'ciudad'=>$this->input->post('ciudad'),	
			'nacimiento'=>$this->input->post('nacimiento'),	
			'password'=>$passwordEncriptado ,	

		);

		$data = $this->model->insertar_usuario($data,$id_codigo,$correo,$codigo);					
		echo json_encode($data);


	}
}
