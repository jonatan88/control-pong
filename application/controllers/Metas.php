<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Mexico_City');

class Metas extends CI_Controller { 
	public function __construct(){
		parent::__construct();            
		$this->load->helper(array('html', 'url', 'date'));
		$this->load->library(array('form_validation')); 
		$this->load->model('Model_1');
		$this->load->library('session');

		
	}
	
	public function mostrarVistas($vista_nombre, $data) {		 
		$data['assets'] = $this->load->view('assets',NULL,TRUE);
		$data['menu'] = $this->load->view('menu',$data,TRUE);
		$data['footer'] = $this->load->view('footer',NULL,TRUE);

		$this->load->view($vista_nombre, $data);  
	}
	
	public function index()
	{ 
		$this->load->library('session');
		
		if($this->session->userdata('is_logged_in')) {			
			$data['nombre_usuario'] = $this->session->userdata('nombre'); 
			$this->mostrarVistas('metas_view',$data);			
		}
		else{
			redirect('Inicio');
		} 
	} 	
}
