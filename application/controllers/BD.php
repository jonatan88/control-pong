<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class BD extends CI_Controller {

 	function __construct(){ 
		parent::__construct();	
		 
		$this->load->helper(array('html', 'url'));	
		$this->load->model('Alumnos_model','tabla');

	} 
	public function mostrarVistas($vista_nombre, $data) {		 
		$data['assets'] = $this->load->view('assets',NULL,TRUE);
	    $data['menu'] = $this->load->view('menu',$data,TRUE);
		$data['footer'] = $this->load->view('footer',NULL,TRUE);
	    $this->load->view($vista_nombre, $data);  
	}

	public function index(){		   	 
		$this->mostrarVistas('alumnos_view',null);
	}
	
	public function ajax_list()
	{
		$list = $this->tabla->get_datatables();
		$data = array();	 
		foreach ($list as $campos) {
		 
			$row = array();
	$row[] = $campos->idEstudiantes;
$row[] = $campos->dni;
$row[] = $campos->correo;
$row[] = $campos->nombre;
$row[] = $campos->apellido;
$row[] = $campos->sede;
$row[] = $campos->grado.'°';

	if($campos->boleta == 1){
		$boleta = "Si";
	}else{
		$boleta = "No";		
	}
	
	if($campos->factura == 1){
		$factura = "Si";
	}else{
		$factura = "No";		
	}
	
$row[] = $boleta;
$row[] = $factura;
$row[] = $campos->comprobante;
	 
	 if($campos->estatus == 1){
		$estatus = "Activo";
	}else{
		$estatus = "Inactivo";		
	}
	
			$row[] = $estatus;
			$row[] = $campos->codigo;
			$row[] = $campos->alumnos_tp;
 

			//add html for action
			//$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Editar" onclick="edit_person('.$campos->idEstudiantes.')"><i class="glyphicon glyphicon-pencil"></i> Editar</a>				  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Eliminar" onclick="delete_person('.$campos->idEstudiantes.')"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->tabla->count_all(),
						"recordsFiltered" => $this->tabla->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	public function ajax_edit($id)
	{
		$data = $this->tabla->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		 
		$data = array(
		
'dni' => $this->input->post('dni'),
'correo' => $this->input->post('correo'),
'nombre' => $this->input->post('nombre'),
'apellido' => $this->input->post('apellido'),
'sede' => $this->input->post('sede'),
'grado' => $this->input->post('grado'),
'boleta' => $this->input->post('boleta'),
'factura' => $this->input->post('factura'),
'comprobante' => $this->input->post('comprobante'),
		
			 	'estatus' => $this->input->post('estatus')
			);
		$insert = $this->tabla->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
			
			
'dni' => $this->input->post('dni'),
'correo' => $this->input->post('correo'),
'nombre' => $this->input->post('nombre'),
'apellido' => $this->input->post('apellido'),
'sede' => $this->input->post('sede'),
'grado' => $this->input->post('grado'),
'boleta' => $this->input->post('boleta'),
'factura' => $this->input->post('factura'),
'comprobante' => $this->input->post('comprobante'),
	
		
				'estatus' => $this->input->post('estatus')
			);
		$this->tabla->update(array('idEstudiantes' => $this->input->post('idEstudiantes')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->tabla->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	} 
}
