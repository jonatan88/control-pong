<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actividades extends CI_Controller {



	public function __construct(){
		parent::__construct();            
		$this->load->helper(array('html', 'url', 'date'));
		$this->load->model('Model_actividades', 'model', TRUE);
		$this->load->library(array('form_validation'));  
		$this->load->library('session');		
	}
	
	public function mostrarVistas($vista_nombre, $data) {		 
		$data['assets'] = $this->load->view('assets',NULL,TRUE);
		$data['menu'] = $this->load->view('menu',$data,TRUE);
		$data['footer'] = $this->load->view('footer',NULL,TRUE);
		$this->load->view($vista_nombre, $data);  
	}
	
	public function index()
	{ 
		$this->load->library('session');
		if($this->session->userdata('is_logged_in')) {			
			$data['nombre_usuario'] = $this->session->userdata('nombre'); 
			$data['ListaProyectos'] = $this->model->ListaProyectos();
			$data['ListaColaborador'] = $this->model->ListaColaborador();
			$data['nombre_colaborador'] =  $this->session->userdata("nombre");

			$this->mostrarVistas('actividades_view',$data);	 
		}
		else{
			redirect('Inicio');
		} 
	}

	public function enviarDatos() { 
		$data = array(
			'fecha'=>$this->input->post('fecha'),	
			'fecha_fin'=>$this->input->post('fecha_fin'),	
			'horas'=>$this->input->post('horas'),
			'observaciones'=>$this->input->post('observaciones'),
			'ac_estatus'=>$this->input->post('ac_estatus'),
			'proyectos_idProyectos'=>$this->input->post('proyectos_idProyectos'),
			'usuarios_idUsuarios'=>$this->input->post('usuarios_idUsuarios')
			//'usuarios_idUsuarios'=>$this->session->userdata('idUsuarios')
 

		);
		$data = $this->model->insertar($data);					
		echo json_encode(array("status" => true));
	} 




	public function obtener_datos()
	{
		$list = $this->model->traer_datos();
		$data = array();


		foreach ($list as $fila) {
			$row = array();

	 
			switch ($fila->ac_estatus) {
				case '0':
				$estatus = "Sin iniciar";				
				break;
				case '1':
				$estatus = "En proceso";				
				break;
				case '2':
				$estatus = "Terminado";				
				break;
				case '3':
				$estatus = "Esperando otra persona";				
				break;
				case '4':
				$estatus = "Aplazada";				
				break;				
				default:
				$estatus = "Sin definir";
				break;
			}		


			$row[] = $fila->proyecto;
			$row[] = $fila->observaciones;
 			$row[] = $fila->fecha;
			$row[] = $fila->fecha_fin;
			$row[] = $fila->horas;
			$row[] = $fila->nombre;
			$row[] = $estatus;
			$row[] = $fila->ac_tp;
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void();" title="Editar" onclick="editar(' . $fila->idActividades . ');">  <i class="material-icons">edit</i> </a> ';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model->count_all(),
			"recordsFiltered" => $this->model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function ajax_edit($id) {
		$data = $this->model->get_by_id($id);
		echo json_encode($data);
	}

	public function actualizarDatos() {
		$data = array(
			'fecha'=>$this->input->post('fecha'),	
			'fecha_fin'=>$this->input->post('fecha_fin'),	
			'horas'=>$this->input->post('horas'),
			'observaciones'=>$this->input->post('observaciones'),
			'ac_estatus'=>$this->input->post('ac_estatus'),
			'proyectos_idProyectos'=>$this->input->post('proyectos_idProyectos'),	 
			'usuarios_idUsuarios'=>$this->input->post('usuarios_idUsuarios')	 
		);
		$this->model->update(array('idActividades' => $this->input->post('idActividades')), $data);
		echo json_encode(array("status" => true));
	}

}
