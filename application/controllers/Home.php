<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller { 
	public function __construct(){
		parent::__construct();            
		$this->load->helper(array('html', 'url', 'date'));
		$this->load->library(array('form_validation')); 
		$this->load->model('Model_home', 'model', TRUE);

	}
	
	public function mostrarVistas($vista_nombre, $data) {		 
		$data['assets'] = $this->load->view('assets',NULL,TRUE);
		$data['menu'] = $this->load->view('menu',$data,TRUE);
		$data['footer'] = $this->load->view('footer',NULL,TRUE);
		$this->load->view($vista_nombre, $data);  
	}
	

	public function index()
	{ 
		$this->load->library('session');
		if($this->session->userdata('is_logged_in')) {			
			$data['nombre_usuario'] = $this->session->userdata('nombre'); 
			$data['fila_colaborador'] = $this->model->traer_usuarios_actividades(); 
			$data['fila_usuarios'] = $this->model->traer_usuarios(); 
			$data['fila_actividades_por_proyectos'] = $this->model->traer_actividades_por_proyectos(); 
			$data['fila_proyectos_por_colaborador'] = $this->model->traer_proyectos_por_colaborador(); 
			$data['fila_usuarios_por_proyecto'] = $this->model->traer_usuarios_por_proyecto(); 
			$data['nombre_colaborador'] =  $this->session->userdata("nombre");

			$this->mostrarVistas('home_view',$data);	 
		}
		else{
			redirect('Inicio');
		} 
	} 

}
