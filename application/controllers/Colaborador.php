<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ E_DEPRECATED);
class Colaborador extends CI_Controller {



	public function __construct(){
		parent::__construct();            
		$this->load->helper(array('html', 'url', 'date'));
		$this->load->model('Model_colaborador', 'model', TRUE);
		$this->load->library(array('form_validation'));  
		$this->load->library('session');	
		$this->load->library('encrypt');

	}
	
	public function mostrarVistas($vista_nombre, $data) {		 
		$data['assets'] = $this->load->view('assets',NULL,TRUE);
		$data['menu'] = $this->load->view('menu',$data,TRUE);
		$data['footer'] = $this->load->view('footer',NULL,TRUE);
		$this->load->view($vista_nombre, $data);  
	}
	
	public function index()
	{ 
		$this->load->library('session');
		if($this->session->userdata('is_logged_in')) {			
			$data['nombre_usuario'] = $this->session->userdata('nombre'); 
			$data['nombre_colaborador'] =  $this->session->userdata("nombre");

			$this->mostrarVistas('colaborador_view',$data);	 
		}
		else{
			redirect('Inicio');
		} 
	}

	public function enviarDatos() { 

		$passwordEncriptado = $this->encrypt->encode($this->input->post('password'));

		$data = array(
			'nombre'=>$this->input->post('nombre'),	
			'correo'=>$this->input->post('correo'),
			'password'=>$passwordEncriptado,
			'privilegios'=>$this->input->post('privilegios'),
			'us_estatus'=>$this->input->post('us_estatus')
		);
		$data = $this->model->insertar($data);					
		echo json_encode(array("status" => true));
	} 




	public function obtener_datos()
	{
		$list = $this->model->traer_datos();
		$data = array();


		foreach ($list as $fila) {
			$row = array();
			if ($fila->us_estatus == 1) {
				$estatus = "Activo";
			}else{
				$estatus = "Inactivo";
			}			

			if ($fila->privilegios == 1) {
				$privilegios = "Administrador";
			}else{
				$privilegios = "Colaborador";
			}			
			$passwordDecifrado = $this->encrypt->decode($fila->password);

			$row[] = $fila->idUsuarios;
			$row[] = $fila->nombre;
			$row[] = $fila->correo;
			$row[] = $passwordDecifrado;
			$row[] = $privilegios;
			$row[] = $estatus;
			$row[] = $fila->us_tp;
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void();" title="Editar" onclick="editar(' . $fila->idUsuarios . ');">  <i class="material-icons">edit</i> </a> ';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model->count_all(),
			"recordsFiltered" => $this->model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function ajax_edit($id) {
		$data = $this->model->get_by_id($id);
		echo json_encode($data);
	}

	public function actualizarDatos() {

		if($this->input->post('password') != ''){

			$passwordEncriptado = $this->encrypt->encode($this->input->post('password'));

			$data = array(
				'nombre'=>$this->input->post('nombre'),	
				'correo'=>$this->input->post('correo'),
				'password'=>$passwordEncriptado,
				'privilegios'=>$this->input->post('privilegios'),
				'color_usuario'=>$this->input->post('color_usuario'),
				'us_estatus'=>$this->input->post('us_estatus')		 
			);


		} else{

			$data = array(
				'nombre'=>$this->input->post('nombre'),	
				'correo'=>$this->input->post('correo'),
				'privilegios'=>$this->input->post('privilegios'),
				'color_usuario'=>$this->input->post('color_usuario'),
				'us_estatus'=>$this->input->post('us_estatus')		 
			);

		}

		$this->model->update(array('idUsuarios' => $this->input->post('idUsuarios')), $data);
		echo json_encode(array("status" => true));
	}

}
