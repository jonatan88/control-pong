<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {



	public function __construct(){
		parent::__construct();            
		$this->load->helper(array('html', 'url', 'date'));
		$this->load->model('Model_clientes', 'model', TRUE);
		$this->load->library(array('form_validation'));  
		$this->load->library('session');		
	}
	
	public function mostrarVistas($vista_nombre, $data) {		 
		$data['assets'] = $this->load->view('assets',NULL,TRUE);
		$data['menu'] = $this->load->view('menu',$data,TRUE);
		$data['footer'] = $this->load->view('footer',NULL,TRUE);
		$this->load->view($vista_nombre, $data);  
	}
	
	public function index()
	{ 
		$this->load->library('session');
		if($this->session->userdata('is_logged_in')) {			
			$data['nombre_usuario'] = $this->session->userdata('nombre');
			$data['nombre_colaborador'] =  $this->session->userdata("nombre");
			
			$this->mostrarVistas('clientes_view',$data);	 
		}
		else{
			redirect('Inicio');
		} 
	}

	public function enviarDatos() { 
		$data = array(
			'cliente'=>$this->input->post('cliente'),	
			'contacto'=>$this->input->post('contacto'),
			'cli_estatus'=>$this->input->post('cli_estatus')
		);
		$data = $this->model->insertar_cliente($data);					
		echo json_encode(array("status" => true));
	} 




	public function obtener_datos()
	{
		$list = $this->model->traer_datos();
		$data = array();


		foreach ($list as $fila) {
			$row = array();
			if ($fila->cli_estatus == 1) {
				$estatus = "Activo";
			}else{
				$estatus = "Inactivo";
			}			

			$row[] = $fila->idClientes;
			$row[] = $fila->cliente;
			$row[] = $fila->contacto;
			$row[] = $estatus;
			$row[] = $fila->cli_tp;
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void();" title="Editar" onclick="editar(' . $fila->idClientes . ');">  <i class="material-icons">edit</i> </a> ';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model->count_all(),
			"recordsFiltered" => $this->model->count_filtered(),
			"data" => $data,
		);
        //output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id) {
		$data = $this->model->get_by_id($id);
		echo json_encode($data);
	}

	public function actualizarDatos() {
		$data = array(
			'cliente' => $this->input->post('cliente'),
			'contacto' => $this->input->post('contacto'),
			'cli_estatus' => $this->input->post('cli_estatus')			 
		);
		$this->model->update(array('idClientes' => $this->input->post('idClientes')), $data);
		echo json_encode(array("status" => true));
	}

}
