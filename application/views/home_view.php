<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
	<title>SUA</title>

	<style type="text/css">

	</style>
	<?=$assets;?>

	<link href='<?=base_url();?>media/js/packages/core/main.css' rel='stylesheet' />
	<link href='<?=base_url();?>media/js/packages/daygrid/main.css' rel='stylesheet' />
	<script src='<?=base_url();?>media/js/packages/core/main.js'></script>
	<script src='<?=base_url();?>media/js/packages/interaction/main.js'></script>
	<script src='<?=base_url();?>media/js/packages/daygrid/main.js'></script>
	<script src='<?=base_url();?>media/js/packages/popper.min.js'></script>
	<script src='<?=base_url();?>media/js/packages/tooltip.min.js'></script>
	<script src='<?=base_url();?>media/js/packages/es.js'></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script src='<?=base_url();?>media/js/graficas.js'></script>


	<script type="text/javascript">
		var calendar = null;
		var id_colaborador = 'all';
		jQuery(document).ready(function($) {

			table = $('#tabla_horas_proyecto').DataTable({
				paging: false,
				info: false
			});
			$.fn.dataTable.ext.search.push(
				function(settings, data, dataIndex) {
					var min = $('#min-date').val();
					var max = $('#max-date').val();
					var createdAt = data[3] || 0;  

					if (
						(min == "" || max == "") ||
						(moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
						) {
						return true;
				}
				return false;
			}
			);

			$('.date-range-filter').change(function() {
				table.draw();
			});

			tabla_colaborador_proyecto = $('#tabla_colaborador_proyecto').DataTable({
				paging: false,
				info: false
			});
			$.fn.dataTable.ext.search.push(
				function(settings, data, dataIndex) {
					var min = $('#min-date2').val();
					var max = $('#max-date2').val();
					var createdAt = data[3] || 0;  

					if (
						(min == "" || max == "") ||
						(moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
						) {
						return true;
				}
				return false;
			}
			);

			$('.rangos_colaborador_proyecto').change(function() {
				tabla_colaborador_proyecto.draw();
			});




			$('.fecha_rangos,.rangos_colaborador_proyecto').datepicker({
				autoClose: true,
				i18n: {
					months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
					monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
					weekdays: ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
					weekdaysShort: ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
					weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"],
					cancel:'Cancelar',
					clear:'Limpiar',
					done:'Aceptar'
				}	

			});


			$('.fecha_filtro_calendario').datepicker({
				autoClose: true,
				i18n: {
					months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
					monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
					weekdays: ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
					weekdaysShort: ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
					weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"],
					cancel:'Cancelar',
					clear:'Limpiar',
					done:'Aceptar'
				}	
			});

		});
		

		document.addEventListener('DOMContentLoaded', function() {
			var calendarEl = document.getElementById('calendar');

			calendar = new FullCalendar.Calendar(calendarEl, {

				eventClick: function(info) {
					info.jsEvent.preventDefault();  

					if (info.event.url) {
						window.open(info.event.url);
					}
					info.el.style.borderColor = 'red';
				},
				plugins: [ 'interaction', 'dayGrid' ],
				header: {
					left: 'prev,next, today',
					center: 'title',
					right: 'dayGridMonth,dayGridWeek,dayGridDay'
				},

				defaultView: '',
				locale: 'es',
				height: 'auto',

				navLinks: true,  
				editable: false,
				eventLimit: false,  
				disableDragging: true,
				eventRender: function(info) {
					console.log(info);
					var tooltip = new Tooltip(info.el, {
						title: info.event.extendedProps.description,
						placement: 'top',
						trigger: 'hover',
						container: 'body'
					});
					return ['all', info.event.extendedProps.colaborador].indexOf(id_colaborador) >= 0
				}, 
				events: [


				<?php  

				if ($fila_colaborador == null) {
					echo "";
				}else{

					foreach($fila_colaborador as $row): ?>
						{
							title: '<?php echo $row->proyecto; ?>',
							start: '<?php echo $row->fecha; ?>',
							end: '<?php echo $row->fecha_fin; ?>',
							url: '<?php echo site_url('Actividades'); ?>',
							//color: Obtener_estatus(<?php echo $row->ac_estatus; ?>),
							color: '#<?php echo $row->color_usuario; ?>',
							description: '<?php echo $row->observaciones; ?> - <?php echo $row->nombre; ?> - <?php echo $row->horas; ?> hr',
							eventColor: '',
							colaborador: '<?php echo $row->idUsuarios; ?>'
						},

					<?php endforeach;  
				}
				?>
				]

			});

			calendar.render();

			$('.selector_colaborador_todos').on('click',function(){
				$('.selector_colaborador').removeClass('borde_1');
				$('.selector_colaborador').removeClass('parpadea');
			});


			$('.selector_colaborador').on('click',function(){
				if($(this).hasClass('borde_1')){
					//$(this).removeClass('borde_1')
					$('.selector_colaborador').removeClass('borde_1');
					$('.selector_colaborador').removeClass('parpadea');

				}
				else
				{
					$('.selector_colaborador').removeClass('borde_1');
					$('.selector_colaborador').removeClass('parpadea');
					$(this).addClass('borde_1 parpadea');

				}

			});
			$('#colaborador_selector').on('change',function(){
				calendar.rerenderEvents();

			});

			$('.fecha_filtro_calendario').change(function() {
				calendar.setOption('visibleRange', {
					start: '2017-04-01',
					end: '2017-04-05'
				});

				console.log("boton");
			});


		});


		function filtrar_colaborador(id){
			id_colaborador = id;
			console.log(id_colaborador);


			calendar.rerenderEvents();

		}

		function Obtener_estatus(estatus){



			switch (estatus) {
				case 0:
				return "#F44336";				
				break;
				case 1:
				return "#0080a4";				
				break;
				case 2:
				return "#4CAF50";				
				break;
				case 3:
				return "#F44336";				
				break;
				case 4:
				return "#gray";				
				break;				
				default:
				return "black";				
				break;
			}		


		}

	</script>
</head>

<style type="text/css">

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}
</style>
<body>


	<div class="container">

		<?=$menu;?>

		<h4>Reportes</h4>
		<p>Calendario por proyecto</p>



		<div class="row">	


			<div class="col m2 s12">


				<div class="fc-content mano selector_colaborador_todos" onclick="filtrar_colaborador('all')" style="background: white; color: black;    margin: 2px;    padding: 3px;    border-radius: 5px;">
					<span class="fc-title" >
						Todos							
					</span>
				</div>


				<?php  

				if ($fila_usuarios == null) {
					echo "";
				}else{

					foreach($fila_usuarios as $row): ?>


						<div class="fc-content mano selector_colaborador" onclick="filtrar_colaborador('<?php echo $row->idUsuarios; ?>')" style="background: #<?php echo $row->color_usuario; ?>; color: white;    margin: 2px;    padding: 3px;    border-radius: 5px;">
							<span class="fc-title" >
								<?php echo $row->nombre; ?>								
							</span>
						</div>


					<?php endforeach;  
				}
				?>
			</div>	 
			<div class="col m10 s12">
				<div id='calendar'></div>
			</div>
		</div>



<!--
		<table>
			<tr>
				<th>Colaborador</th>
			</tr>
			<?php  

			if ($fila_colaborador == null) {
				echo "Sin datos.";
			}else{

				foreach($fila_colaborador as $row): ?>
					<tr>   
						<td class=""><?php echo $row->nombre; ?></td>  								 
					</tr>
				<?php endforeach;  
			}
			?>

		</table>
	-->

	<div class="row">
		<div class="col s12 m6">
			<h4>Horas por proyectos</h4>
			<div id="grafica_proyectos_totales" style="max-width: 900px; height: 300px;"></div>
		</div>


		<div class="col s12 m6">
			<h4>Horas por colaborador</h4>
			<div id="grafica_proyectos_colaboradores" style="max-width: 900px; height: 300px;"></div>
		</div>
	</div>



	
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>


	<h4>Horas dedicadas por colaborador a un proyecto</h4>	

	<div class="row">
		<div class="col s3">
			<p>Filtro por rango de fechas</p>
		</div>
		<div class="col s2">
			<input type="text" id="min-date2" class="form-control  rangos_colaborador_proyecto" data-date-format="yyyy-mm-dd" placeholder="Fecha inicio:">
		</div>
		<div class="col s2">			
			<input type="text" id="max-date2" class="form-control  rangos_colaborador_proyecto" data-date-format="yyyy-mm-dd" placeholder="Fecha fin:">
		</div>
	</div>

	<table class="tabla_horas responsive-table highlight" id="tabla_colaborador_proyecto">
		<thead>
			<tr>
				<th>Colaborador</th>
				<th>Proyecto</th>
				<th>Horas</th>
				<th>Fecha Inicio</th>
				<th>Fecha Fin</th>
			</tr>
		</thead>
		<tbody>
			<?php  

			if ($fila_usuarios_por_proyecto == null) {
				echo "Sin datos.";
			}else{

				foreach($fila_usuarios_por_proyecto as $row): ?>
					<tr>   			
						<td class=""><?php echo $row->nombre; ?></td>  								 
						<td class=""><?php echo $row->proyecto; ?></td>  								 
						<td class=""><?php echo $row->horas; ?></td>  								 
						<td class=""><?php echo $row->fecha; ?></td>  								 
						<td class=""><?php echo $row->fecha_fin; ?></td>  								 
					</tr>
				<?php endforeach;  
			}
			?> 
		</tbody>
	</table>

	<br>
	<br>
	<h4>Horas por proyecto</h4>

	<div class="row">
		<div class="col s3">
			<p>Filtro por rango de fechas</p>
		</div>
		<div class="col s2">
			<input type="text" id="min-date" class="form-control date-range-filter fecha_rangos" data-date-format="yyyy-mm-dd" placeholder="Fecha inicio:">
		</div>
		<div class="col s2">			
			<input type="text" id="max-date" class="form-control date-range-filter fecha_rangos" data-date-format="yyyy-mm-dd" placeholder="Fecha fin:">
		</div>
	</div>
	<table class="tabla_horas responsive-table highlight" id="tabla_horas_proyecto">
		<thead>
			<tr>
				<th>Cliente</th>
				<th>Proyecto</th>
				<th>Horas</th>
				<th>Fecha de Inicio</th>
			</tr>
		</thead>
		<tbody>
			<?php  

			if ($fila_actividades_por_proyectos == null) {
				echo "Sin datos.";
			}else{

				foreach($fila_actividades_por_proyectos as $row): ?>
					<tr>   			
						<td class=""><?php echo $row->cliente; ?></td>  								 
						<td class=""><?php echo $row->proyecto; ?></td>  								 
						<td class=""><?php echo $row->horas; ?></td>  								 
						<td class=""><?php echo $row->fecha; ?></td>  								 
					</tr>
				<?php endforeach;  
			}
			?> 
		</tbody>
	</table>
	<script type="text/javascript">
		google.charts.load("current", {packages:['corechart']});
		google.charts.setOnLoadCallback(drawChart);
		function drawChart() {
			var data = google.visualization.arrayToDataTable([
				["Proyecto", "Horas", { role: "style" } ],

				<?php  

				if ($fila_actividades_por_proyectos == null) {
					echo "";
				}else{

					foreach($fila_actividades_por_proyectos as $row): ?>
						["<?php echo $row->proyecto; ?>", <?php echo $row->horas; ?>, ""],  			

					<?php endforeach;  
				}
				?>  
				]);

			var view = new google.visualization.DataView(data);
			view.setColumns([0, 1,
				{ calc: "stringify",
				sourceColumn: 1,
				type: "string",
				role: "annotation" },
				2]);

			var options = {

				width: 600,
				height: 400,

				bar: {groupWidth: "95%"},
				legend: { position: 'top' },
			};
			var chart = new google.visualization.ColumnChart(document.getElementById("grafica_proyectos_totales"));
			chart.draw(view, options);
		}
	</script>


	<script type="text/javascript">
		google.charts.load("current", {packages:['corechart']});
		google.charts.setOnLoadCallback(drawChart);
		function drawChart() {
			var data = google.visualization.arrayToDataTable([
				["Colaborador", "Horas", { role: "style" } ],
				<?php  
				if ($fila_proyectos_por_colaborador == null) {
					echo "";
				}else{

					foreach($fila_proyectos_por_colaborador as $row): ?>
						["<?php echo $row->nombre; ?>", <?php echo $row->horas; ?>, ""], 
					<?php endforeach;  
				}
				?>  
				]);
			var view = new google.visualization.DataView(data);
			view.setColumns([0, 1,
				{ calc: "stringify",
				sourceColumn: 1,
				type: "string",
				role: "annotation" },
				2]);

			var options = {

				width: 600,
				height: 400,

				bar: {groupWidth: "95%"},
				legend: { position: 'top' },
			};
			var chart = new google.visualization.ColumnChart(document.getElementById("grafica_proyectos_colaboradores"));
			chart.draw(view, options);
		}
	</script>
	<br>
	<br>



</div>


<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.fc-dayGridMonth-button').text('mes');
		$('.fc-dayGridWeek-button').text('semana');
		$('.fc-dayGridDay-button').text('día');
		$('.fc-today-button').text('hoy');
	});
</script>
</body>
</html>