<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
	<title>SUA</title>

	<style type="text/css">
		.colorpicker{
			z-index: 99999 !important;

		}

		.colorpicker input{
			top: -10px !important;
			font-size: 10px !important;
		}
	</style>
	<?=$assets;?>
	<link rel="stylesheet" href="<?=base_url();?>media/js/color/css/colorpicker.css">

	<script src="<?=base_url();?>media/js/color/js/colorpicker.js"></script>

	<script> 
		var save_method; 
		var table;
		$(document).ready(function($) {



			$('#color_usuario').ColorPicker({
				onSubmit: function(hsb, hex, rgb, el) {
					$(el).val(hex);
					$(el).ColorPickerHide();
				},
				onBeforeShow: function () {
					$(this).ColorPickerSetColor(this.value);
				}
			})
			.bind('keyup', function(){
				$(this).ColorPickerSetColor(this.value);
			});



			table = $('#tabla').DataTable({ 
				"language":{
					"decimal":        "",
					"emptyTable":     "Sin datos",
					"info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
					"infoEmpty":      "Mostrando 0 a 0 de 0 registros",
					"infoFiltered":   "( _MAX_ registros filtrados)",
					"infoPostFix":    "",
					"thousands":      ",",
					"lengthMenu":     " ",
					"loadingRecords": "Cargando...",
					"processing":     "Procesando...",
					"search":         "Buscar:",
					"zeroRecords":    "No se han encontrado registros.",
					"paginate": {
						"first":      "Primero",
						"last":       "Último",
						"next":       "Siguiente",
						"previous":   "Anterior"
					},
					"aria": {
						"sortAscending":  ": activate to sort column ascending",
						"sortDescending": ": activate to sort column descending"
					}
				},

				"processing": true, 
				"serverSide": true,  
				"order": [],  

				"ajax": {
					"url": "<?php echo site_url('Colaborador/obtener_datos') ?>",
					"type": "POST" 
				},

				"columnDefs": [
				{ 
					"targets": [ 0 ],  
					"orderable": false,  
				},
				],

			});

			$('.enviar_datos').on('click', function () {
				save();
			});


			$('.modal-trigger').click(function(event) {
				$('form')[0].reset();  
			});
		});	 	


		function save() {
			var url;


			idUsuarios = $.trim($('#idUsuarios').val());
			nombre = $.trim($('#nombre').val());
			correo = $.trim($('#correo').val());
			password = $.trim($('#password').val());
			privilegios = $.trim($('#privilegios').val());
			color_usuario = $.trim($('#color_usuario').val());
			us_estatus = $.trim($('#us_estatus').val());


			if (save_method == 'add') {
				url = "<?php echo site_url('Colaborador/enviarDatos') ?>";

			} else {

				url = "<?php echo site_url('Colaborador/actualizarDatos') ?>";

			}




			if(nombre == "" || correo == "" || privilegios == ""  || color_usuario == "" || us_estatus == ""){
				M.toast({html: 'Completa los datos.'});
			}else{

				jQuery.ajax({
					type: "POST",
					url: url,
					data: $('#form').serialize(),
					dataType: "JSON",

					success: function(data) {	
					//console.log(data);				 						

					if (save_method == 'add') {
						M.toast({html: 'Guardado correctamente.'});
					} else {

						M.toast({html: 'Actualizado correctamente.'});

					}

					table.ajax.reload(); 


					$('.modal').modal('close');
					$('input').val(""); 
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					M.toast({html: 'Error al guardar.'});
					M.toast({html: textStatus});
					M.toast({html: errorThrown});

					console.log('Error');
					$('.mensaje_mal').slideDown();
					$('.progress').hide();
				}
			}); 

			}

		}


		function agregar() {
			save_method = 'add';
			$('form')[0].reset(); 
			$('.modal').modal('open'); 
			$('.enviar_datos').text('Guardar');  

		}

		function editar(id) {

			save_method = 'update';
			$('.enviar_datos').text('Actualizar');  

			$('form')[0].reset();  
			$.ajax({
				url: "<?php echo site_url('Colaborador/ajax_edit/') ?>"+ id,
				type: "POST",
				dataType: "JSON",
				success: function(data) {

					$('label').attr({
						class: 'active'
					});


					$('[name="idUsuarios"]').val(data.idUsuarios);
					$('[name="nombre"]').val(data.nombre);
					$('[name="correo"]').val(data.correo);
				//$('[name="password"]').val(data.password);
				$('[name="privilegios"]').val(data.privilegios);
				$('[name="color_usuario"]').val(data.color_usuario);
				$('[name="us_estatus"]').val(data.us_estatus);


				$('select').formSelect();


				$('.modal').modal('open');  
				$('.modal-title').text('Editar'); 
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert('Error al obtener datos.');
			}
		});
		}





	</script>
</head>
<body>


	<div class="container">

		<?=$menu;?>
		
		<h4>Colaboradores</h4>
		<br>

		<a class="btn-floating btn-large  waves-light red fixed-action-btn" onclick="agregar();"><i class="material-icons">add</i></a>

		<!-- Modal -->
		<div id="modal1" class="modal">
			<div class="modal-content">


				<div class="row" style="    width: 500px;    margin: auto;">


					<form class="" id="form">
						<input type="hidden" value="" id="idUsuarios"  name="idUsuarios" />


						<div class="row">
							<div class="input-field col s12 l12 ">
								<input placeholder="" id="nombre" name="nombre" type="text" class="validate">
								<label for="nombre">Nombre</label>
							</div>					
						</div> 

						<div class="row">
							<div class="input-field col s12 l12 ">
								<input placeholder="" id="correo" name="correo" type="text" class="validate">
								<label for="correo">Correo</label>
							</div>					
						</div> 

						<div class="row">
							<div class="input-field col s12 l12 ">
								<input placeholder="" id="password" name="password" type="text"  class="validate">
								<label for="password">Contraseña</label>
							</div>					
						</div> 

						<div class="row">
							<div class="input-field col s12 l12 ">
								<select id="privilegios" name="privilegios">
									<option value="" disabled selected>Selecciona...</option>
									<option value="1">Administrador</option> 
									<option value="2">Colaborador</option> 
								</select>
								<label for="privilegios">Privilegios</label>
							</div>					
						</div>

						<div class="row">
							<div class="input-field col s12 l12 ">

								<input type="text" id="color_usuario" name="color_usuario" value="" placeholder="Elige color y da clic en el círculo de colores para aceptar" />
								<label for="color_usuario">Color para calendario</label>

							</div>					
						</div> 


						<div class="row">
							<div class="input-field col s12 l12 ">
								<select id="us_estatus" name="us_estatus">
									<option value="" disabled selected>Selecciona...</option>
									<option value="1">Activo</option> 
									<option value="0">Inactivo</option> 
								</select>
								<label for="us_estatus">Estatus</label>
							</div>					
						</div> 

					</form>
				</div>

			</div>
			<div class="modal-footer">

				<button class="btn waves-effect waves-light enviar_datos" type="submit" name="action">Guardar
					<i class="material-icons right">send</i>
				</button>

			</div>
		</div>
		<table id="tabla" class="display  ">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nombre</th>
					<th>Correo</th> 
					<th>Password</th> 
					<th>Privilegios</th> 
					<th>Estatus</th> 
					<th>Fecha Alta</th> 
					<th>Editar</th> 
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</body>
</html>