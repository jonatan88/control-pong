<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
	<title>SUA</title>

	<style type="text/css">

</style>
<?=$assets;?>
<script> 
	var save_method; 
	var table;
	$(document).ready(function($) {




		table = $('#tabla').DataTable({ 
			"language":{
				"decimal":        "",
				"emptyTable":     "Sin datos",
				"info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
				"infoEmpty":      "Mostrando 0 a 0 de 0 registros",
				"infoFiltered":   "( _MAX_ registros filtrados)",
				"infoPostFix":    "",
				"thousands":      ",",
				"lengthMenu":     " ",
				"loadingRecords": "Cargando...",
				"processing":     "Procesando...",
				"search":         "Buscar:",
				"zeroRecords":    "No se han encontrado registros.",
				"paginate": {
					"first":      "Primero",
					"last":       "Último",
					"next":       "Siguiente",
					"previous":   "Anterior"
				},
				"aria": {
					"sortAscending":  ": activate to sort column ascending",
					"sortDescending": ": activate to sort column descending"
				}
			},

			"processing": true, 
			"serverSide": true,  
			"order": [],  

			"ajax": {
				"url": "<?php echo site_url('Bitacora/obtener_datos') ?>",
				"type": "POST" 
			},

			"columnDefs": [
			{ 
				"targets": [ 0 ],  
				"orderable": false,  
			},
			],

		});

		$('.enviar_datos').on('click', function () {
			save();
		});


		$('.modal-trigger').click(function(event) {
			$('form')[0].reset();  
		});
	});	 	


	function save() {
		var url;

 
		idUsuarios = $.trim($('#idUsuarios').val());
		nombre = $.trim($('#nombre').val());
		correo = $.trim($('#correo').val());
		password = $.trim($('#password').val());
		privilegios = $.trim($('#privilegios').val());
		us_estatus = $.trim($('#us_estatus').val());
 

		if (save_method == 'add') {
			url = "<?php echo site_url('Bitacora/enviarDatos') ?>";
			
		} else {

			url = "<?php echo site_url('Bitacora/actualizarDatos') ?>";
			
		}
		



		if(nombre == "" || correo == "" || privilegios == "" || us_estatus == ""){
			M.toast({html: 'Completa los datos.'});
		}else{

			jQuery.ajax({
				type: "POST",
				url: url,
				data: $('#form').serialize(),
				dataType: "JSON",

				success: function(data) {	
					//console.log(data);				 						

					if (save_method == 'add') {
						M.toast({html: 'Guardado correctamente.'});
					} else {

						M.toast({html: 'Actualizado correctamente.'});

					}

					table.ajax.reload(); 


					$('.modal').modal('close');
					$('input').val(""); 
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					M.toast({html: 'Error al guardar.'});
					M.toast({html: textStatus});
					M.toast({html: errorThrown});

					console.log('Error');
					$('.mensaje_mal').slideDown();
					$('.progress').hide();
				}
			}); 

		}

	}


	function agregar() {
		save_method = 'add';
		$('form')[0].reset(); 
		$('.modal').modal('open'); 
		$('.enviar_datos').text('Guardar');  

	}

	function editar(id) {

		save_method = 'update';
		$('.enviar_datos').text('Actualizar');  

		$('form')[0].reset();  
		$.ajax({
			url: "<?php echo site_url('Colaborador/ajax_edit/') ?>"+ id,
			type: "POST",
			dataType: "JSON",
			success: function(data) {

				$('label').attr({
					class: 'active'
				});


				$('[name="idUsuarios"]').val(data.idUsuarios);
				$('[name="nombre"]').val(data.nombre);
				$('[name="correo"]').val(data.correo);
				//$('[name="password"]').val(data.password);
				$('[name="privilegios"]').val(data.privilegios);
				$('[name="us_estatus"]').val(data.us_estatus);

 
				$('select').formSelect();


				$('.modal').modal('open');  
				$('.modal-title').text('Editar'); 
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert('Error al obtener datos.');
			}
		});
	}





</script>
</head>
<body>


	<div class="container">

		<?=$menu;?>
		
		<h4>Bitácora</h4>
 
     
		<table id="tabla" class="display  ">
			<thead>
				<tr>
					<th>ID</th>
					<th>Acción</th>
					<th>Usuario</th>  
					<th>Fecha</th> 
 				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</body>
</html>