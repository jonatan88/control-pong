<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
	<title>SUA</title>

	<style type="text/css">

	</style>
	<?=$assets;?>
	
	


	<script> 
		var save_method; 
		var table;
		$(document).ready(function($) {



			table = $('#tabla').DataTable({

				rowCallback: function(row, data, index){
					switch (data[6]) {
						case 'Sin iniciar':
						$(row).find('td:eq(6)').css({
							color: 'white',
							background: '#F44336'
						});			
						break; 
						case 'En proceso':
						$(row).find('td:eq(6)').css({
							color: 'white',
							background: '#0080a4'
						});			
						break; 
						case 'Terminado':
						$(row).find('td:eq(6)').css({
							color: 'white',
							background: '#4CAF50'
						});			
						break; 
						case 'Esperando otra persona':
						$(row).find('td:eq(6)').css({
							color: 'white',
							background: 'gray'
						});			
						break; 
						case 'Aplazada':
						$(row).find('td:eq(6)').css({
							color: 'white',
							background: 'gray'
						});			
						break; 
					}	


				}, 
				"language":{
					"decimal":        "",
					"emptyTable":     "Sin datos",
					"info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
					"infoEmpty":      "Mostrando 0 a 0 de 0 registros",
					"infoFiltered":   "( _MAX_ registros filtrados)",
					"infoPostFix":    "",
					"thousands":      ",",
					"lengthMenu":     " ",
					"loadingRecords": "Cargando...",
					"processing":     "Procesando...",
					"search":         "Buscar:",
					"zeroRecords":    "No se han encontrado registros.",
					"paginate": {
						"first":      "Primero",
						"last":       "Último",
						"next":       "Siguiente",
						"previous":   "Anterior"
					},
					"aria": {
						"sortAscending":  ": activate to sort column ascending",
						"sortDescending": ": activate to sort column descending"
					}
				},

				"processing": true, 
				"serverSide": true,  
				"order": [],  

				"ajax": {
					"url": "<?php echo site_url('Actividades/obtener_datos') ?>",
					"type": "POST" 
				},

				"columnDefs": [
				{ 
					"targets": [ 0 ],  
					"orderable": false,  
				},
				],

			});

			$('.enviar_datos').on('click', function () {
				save();
			});


			$('.modal-trigger').click(function(event) {
				$('form')[0].reset();  
			});

			$('.fecha_picker').datepicker({
				format: 'yyyy-mm-dd',
				autoClose: true,
				i18n: {
					months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
					monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
					weekdays: ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
					weekdaysShort: ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
					weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"],
					cancel:'Cancelar',
					clear:'Limpiar',
					done:'Aceptar'
				}				
			});


		});	 	

//fin ready

function save() {
	var url;


	fecha = $.trim($('#fecha').val());
	fecha_fin = $.trim($('#fecha_fin').val());
	horas = $.trim($('#horas').val());
	observaciones = $.trim($('#observaciones').val());
	ac_estatus = $.trim($('#ac_estatus').val());
	proyectos_idProyectos = $.trim($('#proyectos_idProyectos').val());
	usuarios_idUsuarios = $.trim($('#usuarios_idUsuarios').val());


	if (save_method == 'add') {
		url = "<?php echo site_url('Actividades/enviarDatos') ?>";

	} else {

		url = "<?php echo site_url('Actividades/actualizarDatos') ?>";

	}




	if(fecha == "" || fecha_fin == "" || horas == "" || observaciones == "" || ac_estatus == "" || proyectos_idProyectos == ""|| usuarios_idUsuarios == "" ){
		M.toast({html: 'Completa los datos.'});
	}else{

		jQuery.ajax({
			type: "POST",
			url: url,
			data: $('#form').serialize(),
			dataType: "JSON",

			success: function(data) {	
					//console.log(data);				 						

					if (save_method == 'add') {
						M.toast({html: 'Guardado correctamente.'});
					} else {

						M.toast({html: 'Actualizado correctamente.'});

					}

					table.ajax.reload(); 


					$('#modal1').modal('close');
					$('input').val(""); 
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					M.toast({html: 'Error al guardar.'});
					M.toast({html: textStatus});
					M.toast({html: errorThrown});

					console.log('Error');
					$('.mensaje_mal').slideDown();
					$('.progress').hide();
				}
			}); 

	}

}


function agregar() {
	save_method = 'add';
	$('form')[0].reset(); 
	$('#modal1').modal('open'); 
	$('.enviar_datos').text('Guardar');  

}

function editar(id) {

	save_method = 'update';
	$('.enviar_datos').text('Actualizar');  

	$('form')[0].reset();  
	$.ajax({
		url: "<?php echo site_url('Actividades/ajax_edit/') ?>"+ id,
		type: "POST",
		dataType: "JSON",
		success: function(data) {

			$('label').attr({
				class: 'active'
			});


			$('[name="idActividades"]').val(data.idActividades);
			$('[name="fecha"]').val(data.fecha);
			$('[name="fecha_fin"]').val(data.fecha_fin);
			$('[name="horas"]').val(data.horas);
			$('[name="observaciones"]').val(data.observaciones);
			$('[name="ac_estatus"]').val(data.ac_estatus);
			$('[name="proyectos_idProyectos"]').val(data.proyectos_idProyectos);
			$('[name="usuarios_idUsuarios"]').val(data.usuarios_idUsuarios);


			$('select').formSelect();


			$('#modal1').modal('open');  
			$('.modal-title').text('Editar'); 
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert('Error al obtener datos.');
		}
	});
}





</script>
</head>
<body>


	<div class="container">

		<?=$menu;?>
		
		<h4>Actividades</h4>
	 

		<a class="btn-floating btn-large  waves-light red fixed-action-btn" onclick="agregar();"><i class="material-icons">add</i></a>

		<!-- Modal -->
		<div id="modal1" class="modal">
			<div class="modal-content">


				<div class="row" style="    width: 500px;    margin: auto;">


					<form class="" id="form">
						<input type="hidden" value="" id="idActividades"  name="idActividades" />

						<div class="row">
							<div class="input-field col s12 l12 ">
								<h4>Registro de actividades</h4>
							</div>					
						</div> 

						<div class="row">
							<div class="input-field col s12 l12 ">
								<?php echo form_dropdown('usuarios_idUsuarios', $ListaColaborador, null, 'id="usuarios_idUsuarios" class="form-control input-md"'); ?>
								<label for="usuarios_idUsuarios">Colaborador</label>
							</div>					
						</div> 


						<div class="row">
							<div class="input-field col s12 l12 ">
								<?php echo form_dropdown('proyectos_idProyectos', $ListaProyectos, null, 'id="proyectos_idProyectos" class="form-control input-md"'); ?>
								<label for="proyectos_idProyectos">Proyecto</label>
							</div>					
						</div> 

						<div class="row">
							<div class="input-field col s12 l12 ">
								<input placeholder="" id="observaciones" name="observaciones" placeholder="" type="text" class="validate">
								<label for="observaciones">Actividad</label>
							</div>					
						</div> 


						<div class="row">
							<div class="input-field col s12 l12 ">
								<input placeholder="" id="fecha" name="fecha" type="text" class="fecha_picker" autocomplete="off">
								<label for="fecha">Fecha Inicio</label>
							</div>					
						</div> 

						<div class="row">
							<div class="input-field col s12 l12 ">
								<input placeholder="" id="fecha_fin" name="fecha_fin" type="text" class="fecha_picker" autocomplete="off">
								<label for="fecha_fin">Fecha Fin</label>
							</div>					
						</div> 


						<div class="row">
							<div class="input-field col s12 l12 ">
								<select id="horas" name="horas">
									<option value="" disabled selected>Selecciona...</option>
									<option value="0">0</option> 
									<option value="1">1</option> 
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
								</select>
								<label for="horas">Horas</label>
							</div>					
						</div> 








						<div class="row">
							<div class="input-field col s12 l12 ">
								<select id="ac_estatus" name="ac_estatus">
									<option value="" disabled selected>Selecciona...</option>
									<option value="0">Sin iniciar</option> 
									<option value="1">En proceso</option> 
									<option value="2">Terminado</option> 
									<option value="3">Esperando otra persona</option> 
									<option value="4">Aplazada</option> 
								</select>
								<label for="ac_estatus">Estatus</label>
							</div>					
						</div> 

					</form>
				</div>

			</div>
			<div class="modal-footer">

				<button class="btn waves-effect waves-light enviar_datos" type="submit" name="action">Guardar
					<i class="material-icons right">send</i>
				</button>

			</div>
		</div>
		<table id="tabla" class="display  ">
			<thead>
				<tr>
					<th>Proyecto</th>
					<th>Actividad</th> 
					<th>Fecha Inicio</th>
					<th>Fecha Fin</th>
					<th>Horas</th> 
					<th>Encargado</th> 
					<th>Estatus</th> 
					<th>Fecha Alta</th> 
					<th>Editar</th> 
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</body>
</html>