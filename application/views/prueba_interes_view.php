<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>PEARSON</title>

	<style type="text/css">
	
</style>
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<link rel="stylesheet" href="<?=base_url();?>media/css/style.css">


<!-- Compiled and minified JavaScript --> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="<?=base_url();?>media/js/calculos_tabla_1.js"></script>
<script src="<?=base_url();?>media/js/tabla_intereses_json.js"></script>
<script src="<?=base_url();?>media/js/tabla_personalidad.js"></script>
<script src="<?=base_url();?>media/js/tabla_habilidades.js"></script>
<script src="<?=base_url();?>media/js/calculos_finales.js"></script>

<script src="<?=base_url();?>media/js/funciones.js"></script>
<script src="<?=base_url();?>media/js/jquery.mask.js"></script>


<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=base_url();?>media/js/jquery.ui.touch-punch.min.js"></script>


<script> 
</script>

<style>
 .espacio_celda{
	     border: solid 2px #ffffff;
 }
</style>

</head>
<body>


	<div class="container">
		<div class="row " style="margin-top: -97px;  "> 			
			<div class="input-field col m12" style="height: 110px;">			
				<img src="<?=base_url();?>media/images/banner.png" class="responsive-img" style=" " >
			</div> 
		</div> 
		
		
		

		<div class="row " style=""> 			
			<div class="input-field col s12" style="">

				<?=$menu;?>

				<div style=" max-width: 90%;    margin: auto;">

					<div class="presentacion" >
						<p class="texto_azul_2">
							Presentación
						</p>
						<p>
							Orientación profesional es una propuesta de Pearson que ha contribuido para que miles de jóvenes construyan su plan de vida y carrera a partir del modelo de toma de decisión profesional.
						</p>
						<p>
							Este sitio, es el primero en su tipo, ofrece las mejores herramientas del libro impreso para que los jóvenes se den cuenta de cuáles son los perfiles profesionales afines con sus intereses, aptitudes, valores, habilidades intelectuales y rasgos de personalidad.
						</p>
						<p>
							Así, acercamos a nuestros usuarios una herramienta práctica y novedosa, que podrán completar en una oportunidad o en varias, y les arrojará una interpretación de sus características personales vinculadas con las áreas universitarias más afines a éstas.
						</p>
						<p>
							Estamos seguros de ofrecer la mejor opción de orientación profesional. Pearson ha sido líder en este tema. Les damos la bienvenida y deseamos que el resultado sea enriquecedor.

						</p>


						<div class="center fondo_verde" onclick="ir_a_seccion('instrucciones_prueba');" style=" float: right;width: 200px; cursor:pointer;font-size: 25px;" >
							<span class="">Siguiente</span>
						</div>



					</div>

					<div class="instrucciones_prueba oculto">

						<p class="texto_azul_2">
							Instrucciones
						</p>
						<p>
							<span>El sitio se compone de cinco cuestionarios:</span>
						</p><ul>
							<li>• Intereses</li>
							<li>• Aptitudes</li>
							<li>• Valores</li>
							<li>• Rasgos de personalidad</li>
							<li>• Habilidades intelectuales</li>
						</ul>
						<p></p>
						<p>
						</p><ol>
							<li>Lee cuidadosamente las instrucciones antes de responder.</li>
							<li>Cada cuestionario te arrojará resultados parciales.</li>
							<li>Al final, esos resultados se integrarán para formar tu perfil, éste se correlacionará con las áreas profesionales sugiriéndote carreras específicas afines a tu perfil.</li>
						</ol>
						<p></p>
						<p>
							<span>Importante:</span>
						</p><ul>

							<li>Los cuestionarios deben realizarse en el orden indicado.</li>
						</ul>
						<p>
							<div class="center fondo_verde" onclick="ir_a_seccion('seccion_1_numero_1');" style="float: right;width: 200px; cursor:pointer;font-size: 25px;" >
								<span class="">Siguiente</span>
							</div>

						</p>




					</div>


					<div class="row">

						<div class="col s12" style="text-align: justify;">
							<p class="instrucciones_texto"></p>  
						</div>
					</div>


					<div class="cuestionarios_panel oculto">



						<div class="row">
							<div class="input-field col s4">

								<table border="0" cellpadding="0" cellspacing="0" class="tabla_1">
									<tbody><tr>
										<td colspan="4" class="fondoRojo"><div class="titulo">Cuestionarios</div></td>
									</tr>
									<tr>
										<td colspan="4" class="paneles_titulo titulo_1 fondoPlomoObscuro fondoPlomoClaro" height="45">

											<div class="containerMenu">
												<div class="">
													<span class="tituloMenu">Intereses</span></div>
													<div class="numeros">
														<div class="boxMoradoStep fondo_azul pasos cuadros_gris boxes box_seccion_1_1"  onclick="ir_a_seccion('seccion_1_numero_1')" id="step1"><span class="tituloMenu texto_gris">I</span></div>
														<div class="boxAmarillo fondo_azul pasos  boxes box_seccion_1_2"  onclick="ir_a_seccion('seccion_1_numero_2')" id="step2"><span class="tituloMenu texto_gris">II</span></div>
														<div class="boxPlomoClaro fondo_azul pasos  boxes box_seccion_1_3" onclick="ir_a_seccion('seccion_1_numero_3')" id="step3"><span class="tituloMenu texto_gris">III</span></div>
														<div class="boxPlomo fondo_azul2 pasos  boxes box_seccion_1_4" onclick="ir_a_seccion('seccion_1_numero_4')" id="step4"><span class="tituloMenu texto_gris">IV</span></div>
													</div>

												</div>
											</td>
										</tr>
										<tr> 
											<td colspan="4" class="paneles_titulo titulo_2 fondoPlomoObscuro  " height="45"><div class="tituloMenu">Aptitudes</div>
												<div class="numeros">
													<div class="boxMoradoStep fondo_azul pasos  boxes box_seccion_2_1" onclick="ir_a_seccion('seccion_2_numero_1')" id="step1"><span class="tituloMenu texto_gris">I</span></div>
													<div class="boxAmarillo fondo_azul pasos  boxes box_seccion_2_2"  onclick="ir_a_seccion('seccion_2_numero_2')" id="step2"><span class="tituloMenu texto_gris">II</span></div>
													<div class="boxPlomoClaro fondo_azul pasos  boxes box_seccion_2_3"  onclick="ir_a_seccion('seccion_2_numero_3')" id="step3"><span class="tituloMenu texto_gris">III</span></div>
													<div class="boxPlomo fondo_azul2 pasos  boxes box_seccion_2_4" onclick="ir_a_seccion('seccion_2_numero_4')"  id="step4"><span class="tituloMenu texto_gris">IV</span></div>
												</div>

											</td>


										</tr>
										<tr>
											<td colspan="4" class="paneles_titulo  titulo_3 fondoPlomoObscuro" height="45"><div class="tituloMenu">Valores</div>

												<div class="numeros">
													<div class="boxMoradoStep fondo_azul pasos  boxes box_seccion_3_1"  onclick="ir_a_seccion('seccion_3_numero_1')" id="step1"><span class="tituloMenu texto_gris">I</span></div>
													<div class="boxAmarillo fondo_azul pasos  boxes box_seccion_3_2"  onclick="ir_a_seccion('seccion_3_numero_2')" id="step2"><span class="tituloMenu texto_gris">II</span></div>
													<div class="boxPlomoClaro fondo_azul pasos  boxes box_seccion_3_3"  onclick="ir_a_seccion('seccion_3_numero_3')" id="step3"><span class="tituloMenu texto_gris">III</span></div>
													<div class="boxPlomo fondo_azul2 pasos  boxes box_seccion_3_4"  onclick="ir_a_seccion('seccion_3_numero_4')" id="step4"><span class="tituloMenu texto_gris">IV</span></div>
												</div>
											</td>
										</tr>
										<tr>
											<td colspan="4" class="paneles_titulo  titulo_4 fondoPlomoObscuro" height="45"><div class="tituloMenu">Personalidad</div>
												<div class="numeros">
													<div class="boxMoradoStep fondo_azul pasos   boxes box_seccion_4_1" onclick="ir_a_seccion('seccion_4_numero_1')" id="step1"><span class="tituloMenu texto_gris">I</span></div>
													<div class="boxAmarillo fondo_azul pasos  boxes box_seccion_4_2" onclick="ir_a_seccion('seccion_4_numero_2')" id="step2"><span class="tituloMenu texto_gris">II</span></div>
													<div class="boxPlomoClaro fondo_azul pasos  boxes box_seccion_4_3" onclick="ir_a_seccion('seccion_4_numero_3')" id="step3"><span class="tituloMenu texto_gris">III</span></div>
													<div class="boxPlomo fondo_azul2 pasos  boxes box_seccion_4_4" onclick="ir_a_seccion('seccion_4_numero_4')" id="step4"><span class="tituloMenu texto_gris">IV</span></div>

													<div class="boxPlomo fondo_azul2 pasos  boxes box_seccion_4_5 borde_lados borde_top" onclick="ir_a_seccion('seccion_4_numero_5')" id=""><span class="tituloMenu texto_gris">V</span></div>
													<div class="boxPlomo fondo_azul2 pasos  boxes box_seccion_4_6 borde_lados borde_top" onclick="ir_a_seccion('seccion_4_numero_6')" id=""><span class="tituloMenu texto_gris ">VI</span></div>
													<div class="boxPlomo fondo_azul2 pasos  boxes box_seccion_4_7 borde_lados borde_top" onclick="ir_a_seccion('seccion_4_numero_7')" id=""><span class="tituloMenu texto_gris">VII</span></div>
													<div class="boxPlomo fondo_azul2 pasos  boxes box_seccion_4_8 borde_top" onclick="ir_a_seccion('seccion_4_numero_8')" id=""><span class="tituloMenu texto_gris">VIII</span></div>

												</div>

											</td>
										</tr>
										<tr>
											<td colspan="4" class="paneles_titulo titulo_5 fondoPlomoObscuro" height="45"><div class="tituloMenu">Habilidades intelectuales</div>

												<div class="numeros">

													<div class="boxMoradoStep fondo_azul pasos   boxes box_seccion_5_1" onclick="ir_a_seccion('seccion_5_numero_1')" id="step1"><span class="tituloMenu texto_gris">I</span></div>
													<div class="boxAmarillo fondo_azul pasos  boxes box_seccion_5_2" onclick="ir_a_seccion('seccion_5_numero_2')" id="step2"><span class="tituloMenu texto_gris">II</span></div>
													<div class="boxPlomoClaro fondo_azul pasos  boxes box_seccion_5_3" onclick="ir_a_seccion('seccion_5_numero_3')" id="step3"><span class="tituloMenu texto_gris">III</span></div>
													<div class="boxPlomo fondo_azul2 pasos  boxes box_seccion_5_4" onclick="ir_a_seccion('seccion_5_numero_4')" id="step4"><span class="tituloMenu texto_gris">IV</span></div>

													<div class="boxPlomo fondo_azul2 pasos  boxes box_seccion_5_5 borde_lados borde_top" onclick="ir_a_seccion('seccion_5_numero_5')" id=""><span class="tituloMenu texto_gris">V</span></div>
													<div class="boxPlomo fondo_azul2 pasos  boxes box_seccion_5_6 borde_lados borde_top" onclick="ir_a_seccion('seccion_5_numero_6')" id=""><span class="tituloMenu texto_gris ">VI</span></div>
													<div class="boxPlomo fondo_azul2 pasos  boxes box_seccion_5_7 borde_lados borde_top" onclick="ir_a_seccion('seccion_5_numero_7')" id=""><span class="tituloMenu texto_gris">VII</span></div>
													<div class="boxPlomo fondo_azul2 pasos  boxes box_seccion_5_8 borde_top" onclick="ir_a_seccion('seccion_5_numero_8')" id=""><span class="tituloMenu texto_gris">VIII</span></div>

												</div>

											</td>
										</tr>
									</tbody></table>

										<p>Si deseas navegar por los diferentes cuestionarios, utiliza los recuadros grises de la izquierda. No navegues los cuestionarios sin contestar los reactivos, ya que alterarás tu resultado.</p>

								</div>
								<div class="panel_intereses">
									<?=$intereses_view;?>
								</div>
								<div class="panel_aptitudes">
									<?=$aptitudes_view;?>
								</div>
								<div class="panel_valores">
									<?=$valores_view;?>
								</div>
								<div class="panel_personalidad"> 
									<?=$personalidad_view;?>
								</div>
								<div class="panel_habilidades">
									<?=$habilidades_view;?>
								</div>

							</div>


						</div>

 


					</div>
				</div>
			</div>
		</div>


	</body>
	</html>