<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>PEARSON</title>

	<style type="text/css">
	
</style>
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<link rel="stylesheet" href="<?=base_url();?>media/css/style.css">


<!-- Compiled and minified JavaScript --> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="<?=base_url();?>media/js/calculos_tabla_1.js"></script>
<script src="<?=base_url();?>media/js/tabla_intereses_json.js"></script>
<script src="<?=base_url();?>media/js/tabla_personalidad.js"></script>
<script src="<?=base_url();?>media/js/tabla_habilidades.js"></script>
<script src="<?=base_url();?>media/js/calculos_finales.js"></script>

<script src="<?=base_url();?>media/js/funciones.js"></script>
<script src="<?=base_url();?>media/js/jquery.mask.js"></script>


<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=base_url();?>media/js/jquery.ui.touch-punch.min.js"></script>


<script> 

	function imprSelec(nombre) {
		/*
		var ficha = document.getElementById(nombre);
		var ventimp = window.open(' ', 'popimpr');
		ventimp.document.write( ficha.innerHTML );
		ventimp.document.close();
		ventimp.print( );
		ventimp.close();
		*/

		window.print();
	}


	document.addEventListener('DOMContentLoaded', function() {
		var elems = document.querySelectorAll('.modal');
		var instances = M.Modal.init(elems);
	});

	$(document).ready(function(){	



		$('.modal').modal();

		
		Llenar_datos_resultado_final();


		$( ".enviar_correo" ).click(function() {


			correo_envio = $('#correo_envio').val();

			if(correo_envio != ""){
				contenido_imprimir = $('#contenido_imprimir').html();

				$.ajax({
					url: base_url()+"index.php/Resultados/Enviar_correo",
					type: "POST",
					dataType: "json",
					data: {correo_envio:correo_envio, contenido_imprimir:contenido_imprimir},
					complete: function(response) {
						if(response.responseText === 'success'){
							alert("Los resultados han sido enviados correntamente al correo: " + correo_envio);
							$('#correo_envio').val("");
						} else {
							alert("Error, intenta nuevamente.");
						}		
					}
				});		

			}else{
				alert("Debes escribir un correo.");
			}


		});

		ordenar_habilidades();

	}); 


	function Llenar_datos_resultado_final(){

		<?php

		$interes_mayor_1 =  $fila_cuestionario_intereses[0]->interes_mayor_1;
		$interes_mayor_2 =  $fila_cuestionario_intereses[0]->interes_mayor_2;
		$interes_mayor_3 =  $fila_cuestionario_intereses[0]->interes_mayor_3;

		$valor_mayor_1 =  $fila_cuestionario_valores[0]->valor_mayor_1;
		$valor_mayor_2 =  $fila_cuestionario_valores[0]->valor_mayor_2;

		$personalidad_resultado_1 =  $fila_cuestionario_personalidad[0]->personalidad_resultado_1;
		$personalidad_resultado_2 =  $fila_cuestionario_personalidad[0]->personalidad_resultado_2;

		$habilidad_resultado_1 =  $fila_cuestionario_habilidad[0]->habilidad_resultado_1;
		$habilidad_resultado_2 =  $fila_cuestionario_habilidad[0]->habilidad_resultado_2;
		//echo "$interes_mayor_1";
		if(trim($interes_mayor_1) == 'Debes completar el cuestionario.' || $valor_mayor_1 == '-' || $personalidad_resultado_1 == '0'   || $habilidad_resultado_1 == 'Completa tus respuestas' ){
			
			echo "$('.panel_final').hide();";
			echo "$('.mensaje_final').text('Completa los cuestionarios para ver el resultador final');";
			
		}else{	
			echo "$('.panel_final').show();";
			echo "Calcular_datos('" . $interes_mayor_1 . "','" . $interes_mayor_2 . "','" . $interes_mayor_3 . "','" .$valor_mayor_1 . "','" . $valor_mayor_2 . "','" . $personalidad_resultado_1 . "','" . $personalidad_resultado_2 . "','" . $habilidad_resultado_1 . "','" . $habilidad_resultado_2 . "');";

		}

		?>

	}


	function traer_resultado_intereses_por_id(id){

		$.ajax({
			url: base_url()+"index.php/Resultados/Obtener_res_intereses_id",
			type: "POST",
			dataType: "json",
			data: {id:id},
			success: function(res) {
				if (res)
				{ 
					console.log(res); 
					$('.total_intereses_alto_1').text(res[0].total_intereses_alto_1); 
					$('.interes_mayor_1').text(res[0].interes_mayor_1); 
					$('.caracteristica_mayor_1').text(res[0].caracteristica_mayor_1); 
					$('.total_intereses_alto_2').text(res[0].total_intereses_alto_2); 
					$('.interes_mayor_2').text(res[0].interes_mayor_2); 
					$('.caracteristica_mayor_2').text(res[0].caracteristica_mayor_2); 
					$('.total_intereses_alto_3').text(res[0].total_intereses_alto_3); 
					$('.interes_mayor_3').text(res[0].interes_mayor_3); 
					$('.caracteristica_mayor_3').text(res[0].caracteristica_mayor_3); 
					$('.total_intereses_bajo_1').text(res[0].total_intereses_bajo_1); 
					$('.interes_menor_1').text(res[0].interes_menor_1); 
					$('.caracteristica_menor_1').text(res[0].caracteristica_menor_1); 
					$('.tabla_intereses').show();
				} else {
					alert("Error, intenta nuevamente.");
				}		
			}
		});		
	} 

	function traer_resultado_aptitudes_por_id(id){

		$.ajax({
			url: base_url()+"index.php/Resultados/Obtener_res_aptitudes_id",
			type: "POST",
			dataType: "json",
			data: {id:id},
			success: function(res) {
				if (res)
				{ 

					$('.total_aptitudes_alto_1').text(res[0].total_aptitudes_alto_1); 
					$('.aptitud_mayor_1').text(res[0].aptitud_mayor_1); 
					$('.aptitud_caracteristica_mayor_1').text(res[0].aptitud_caracteristica_mayor_1); 
					$('.total_aptitudes_alto_2').text(res[0].total_aptitudes_alto_2); 
					$('.aptitud_mayor_2').text(res[0].aptitud_mayor_2); 
					$('.aptitud_caracteristica_mayor_2').text(res[0].aptitud_caracteristica_mayor_2); 
					$('.total_aptitudes_alto_3').text(res[0].total_aptitudes_alto_3); 
					$('.aptitud_mayor_3').text(res[0].aptitud_mayor_3); 
					$('.aptitud_caracteristica_mayor_3').text(res[0].aptitud_caracteristica_mayor_3); 
					$('.total_aptitudes_bajo_1').text(res[0].total_aptitudes_bajo_1); 
					$('.aptitudes_menor_1').text(res[0].aptitudes_menor_1); 
					$('.aptitudes_caracteristica_menor_1').text(res[0].aptitudes_caracteristica_menor_1); 
					$('.tabla_aptitudes').show();
				} else {
					alert("Error, intenta nuevamente.");
				}		
			}
		});		
	} 

	
	
	function traer_resultado_valores_por_id(id){

		$.ajax({
			url: base_url()+"index.php/Resultados/Obtener_res_valores_id",
			type: "POST",
			dataType: "json",
			data: {id:id},
			success: function(res) {
				if (res)
				{ 

					$('.idCuestionario_valores').text(res[0].idCuestionario_valores); 
					$('.total_valores_alto_1').text(res[0].total_valores_alto_1); 
					$('.valor_mayor_1').text(res[0].valor_mayor_1); 
					$('.valor_caracteristica_mayor_1').text(res[0].valor_caracteristica_mayor_1); 
					$('.valor_meta_mayor_1').text(res[0].valor_meta_mayor_1 ); 
					$('.total_valores_alto_2').text(res[0].total_valores_alto_2); 
					$('.valor_mayor_2').text(res[0].valor_mayor_2); 
					$('.valor_caracteristica_mayor_2').text(res[0].valor_caracteristica_mayor_2); 
					$('.valor_meta_mayor_2').text(res[0].valor_meta_mayor_2); 
					$('.total_valores_alto_3').text(res[0].total_valores_alto_3); 
					$('.valor_mayor_3').text(res[0].valor_mayor_3 ); 
					$('.valor_caracteristica_mayor_3').text(res[0].valor_caracteristica_mayor_3); 
					$('.valor_meta_mayor_3').text(res[0].valor_meta_mayor_3 ); 
					$('.total_valores_bajo_1').text(res[0].total_valores_bajo_1); 
					$('.valores_menor_1').text(res[0].valores_menor_1); 
					$('.valores_caracteristica_menor_1').text(res[0].valores_caracteristica_menor_1); 
					$('.valores_meta_menor_1').text(res[0].valores_meta_menor_1 ); 

					$('.tabla_valores').show();
				} else {
					alert("Error, intenta nuevamente.");
				}		
			}
		});		
	} 

	
	
	function traer_resultado_personalidad_por_id(id){

		$.ajax({
			url: base_url()+"index.php/Resultados/Obtener_res_personalidad_id",
			type: "POST",
			dataType: "json",
			data: {id:id},
			success: function(res) {
				if (res)
				{ 

					$('.total_extrovertido').text(res[0].total_extrovertido); 
					$('.total_introvertido').text(res[0].total_introvertido); 
					$('.total_juicios').text(res[0].total_juicios); 
					$('.total_percepciones').text(res[0].total_percepciones); 
					$('.total_sensaciones').text(res[0].total_sensaciones); 
					$('.total_intuicion').text(res[0].total_intuicion); 
					$('.total_pensamiento').text(res[0].total_pensamiento); 
					$('.total_sentimientos ').text(res[0].total_sentimientos ); 


					$('.tabla_personalidad').show();
				} else {
					alert("Error, intenta nuevamente.");
				}		
			}
		});		
	} 

	
	
	function traer_resultado_habilidad_por_id (id){

		$.ajax({
			url: base_url()+"index.php/Resultados/Obtener_res_habilidad_id",
			type: "POST",
			dataType: "json",
			data: {id:id},
			success: function(res) {
				if (res)
				{ 
					$('.total_naturalista').text(res[0].total_naturalista); 
					$('.total_intrapersonal').text(res[0].total_intrapersonal); 
					$('.total_interpersonal').text(res[0].total_interpersonal); 
					$('.total_corporal').text(res[0].total_corporal); 
					$('.total_espacial').text(res[0].total_espacial); 
					$('.total_logico_matematica').text(res[0].total_logico_matematica); 
					$('.total_musical').text(res[0].total_musical); 
					$('.total_linguistica').text(res[0].total_linguistica); 
					$('.tabla_habilidades').show();
				} else {
					alert("Error, intenta nuevamente.");
				}		
			}
		});		
	} 

	
	
	
	
</script>

<style>
.espacio_celda{
	border: solid 2px #ffffff;
}

table td{
	text-align: center;
}


.tabla_universidad_resultados td{
	text-align: left ;
}
</style>

</head>
<body>


	<div class="container">
		<div class="row " style="margin-top: -97px;  "> 			
			<div class="input-field col m12" style="height: 110px;">			
				<img src="<?=base_url();?>media/images/banner.png" class="responsive-img" style=" " >
			</div> 
		</div> 
		
		
		

		<div class="row " style=""> 			
			<div class="input-field col s12" style="">

				<?=$menu;?>

				<div style=" max-width: 90%;    margin: auto;">

					<div id="contenido_imprimir" class="">

						<p class="">
							A continuación se enlistan los resultados para cada una de las secciones que haz resuelto.
						</p>
						<p class="">
							<b>Mi nombre:</b> <u><?=$nombre_usuario;?></u>
						</p>
						
						<p class="">
							<b>Mi correo:</b> <u><?=$correo_usuario;?></u>
						</p>
						
						<br>
						<p class="texto_azul_2">
							<b>Carreras</b>
						</p>	 
						<table>
							<tbody>
								<tr>    
									<td class="fondoPlomoClaro espacio_celda" >Carrera 1</td>  
									<td class="fondoPlomoClaro espacio_celda" >Carrera 2</td>  
									<td class="fondoPlomoClaro espacio_celda" >Carrera 3</td>  
									<td class="fondoPlomoObscuro espacio_celda" >Fecha</td> 
								</tr>
								<?php  

								if ($fila_carreras == null) {
									echo "Debes completar el cuestionario de carreras.";
								}else{

									foreach($fila_carreras as $row): ?>
									<tr>   
										<td class="espacio_celda fondo_azul_1"><?php echo $row->carrera_1; ?></td> 
										<td class="espacio_celda fondo_azul_1"><?php echo $row->carrera_2; ?></td> 
										<td class="espacio_celda fondo_azul_1"><?php echo $row->carrera_3; ?></td> 
										<td class="espacio_celda fondo_azul_1"><?php 

										$origDate = $row->tp;

										$newDate = date("d-m-Y H:i:s", strtotime($origDate));

										echo $newDate; ?></td> 										 
									</tr>
								<?php endforeach;  
							}
							?>


						</tbody>
					</table> 

					<br>
					<div class="row" style=" background: #dde3ec; padding: 30px; margin: 1px;"> 	
						<?php  

						if ($fila_carreras == null) {
							echo "Debes completar el cuestionario de universidades.";
						}else{
							foreach($fila_carreras as $row): ?>
							<?php echo $row->carreras_resultados_desglose; ?> 
						<?php endforeach;  
					}
					?>

				</div>
				<br>
				<br>
					<hr>
				<p class="texto_azul_2">
					<b>Universidades</b>
				</p>	 
				<table>
					<tbody>
						<tr>    
							<td class="fondoPlomoClaro espacio_celda" >Universidad 1</td>  
							<td class="fondoPlomoClaro espacio_celda" >Universidad 2</td>  
							<td class="fondoPlomoObscuro espacio_celda" >Fecha</td> 
						</tr>


						<?php  

						if ($fila_universidades == null) {
							echo "Debes completar el cuestionario de universidades.";
						}else{

							foreach($fila_universidades as $row): ?>
							<tr>   
								<td class="espacio_celda fondo_azul_1"><?php echo $row->universidad_1; ?></td> 
								<td class="espacio_celda fondo_azul_1"><?php echo $row->universidad_2; ?></td> 
								<td class="espacio_celda fondo_azul_1"><?php 
								$origDate = $row->tp;

								$newDate = date("d-m-Y H:i:s", strtotime($origDate));

								echo $newDate;
								?></td> 										 
							</tr>
						<?php endforeach;  
					}
					?>


				</tbody>
			</table>


			<br>
			<div class="row" style=" background: #dde3ec; padding: 30px; margin: 1px;"> 	
				<?php  

				if ($fila_universidades == null) {
					echo "Debes completar el cuestionario de universidades.";
				}else{

					foreach($fila_universidades as $row): ?>

					<?php echo $row->universidades_resultados_desglose; ?>  


				<?php endforeach;  
			}
			?>

		</div>

		<br>
		<hr>
		<p class="texto_azul_2">
			<b>Intereses</b>
		</p>	 


		<?php
		if($fila_cuestionario_intereses[0]->total_intereses_bajo_1 == null){
			echo "nulo";

		}
		?>

		<div class="tabla_intereses">					 

			<table border="0" cellpadding="10" cellspacing="0" id="TABLA_RES">
				<tbody>

					<tr >
						<td colspan="3"  style="padding-top: 0px;" class="fondoRojoResultado"><div class="itemConsigna">Intereses predominantes</div></td>
					</tr>
					<tr>
						<td class="fondoPlomoObscuro"><div class="itemConsigna" style="">Puntos</div></td>
						<td class="fondoPlomoClaro"><div class="itemConsigna">Interés</div></td>
						<td class="fondoPlomoObscuro"><div class="itemConsigna" style="text-align:left;">&nbsp;Descripción</div></td>
					</tr>
					<tr>
						<td class="fondo_azul_1" style="border: solid 1px #ffffff;">
							<div class="itemConsigna"><div id="divResPunto1" name="divResPunto1" style="font-size:28px;text-align:center;" class="">

								<?php 
								if ($fila_cuestionario_intereses == null) {
									echo "-";
								}else{
									echo $fila_cuestionario_intereses[0]->total_intereses_alto_1;
								}
								?>

							</div>
						</div>
					</td>
					<td class="fondo_azul_1" style="border: solid 1px #ffffff;">
						<div class="itemConsigna"><div id="divResInteres1" name="divResInteres1" style="font-size:12px;text-align:left;" class="" >

							<?php 
							if ($fila_cuestionario_intereses == null) {
								echo "-";
							}else{
								echo $fila_cuestionario_intereses[0]->interes_mayor_1;
							}
							?>
						</div>
					</div>
				</td>
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;"  >
					<div class="itemConsigna">
						<div id="divResGlosa1" name="divResGlosa1" style="font-size:12px;text-align:left;" class="">

							<?php 
							if ($fila_cuestionario_intereses == null) {
								echo "-";
							}else{
								echo $fila_cuestionario_intereses[0]->interes_mayor_1;
							}
							?>

						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;">
					<div class="itemConsigna"><div id="divResPunto1" name="divResPunto1" style="font-size:28px;text-align:center;" class="">

						<?php 
						if ($fila_cuestionario_intereses == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_intereses[0]->total_intereses_alto_2;
						}
						?>
					</div>
				</div>
			</td>
			<td class="fondo_azul_1" style="border: solid 1px #ffffff;">
				<div class="itemConsigna"><div id="divResInteres1" name="divResInteres1" style="font-size:12px;text-align:left;" class="" >

					<?php 
					if ($fila_cuestionario_intereses == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_intereses[0]->interes_mayor_2;
					}
					?>

				</div>
			</div>
		</td>
		<td class="fondo_azul_1" style="border: solid 1px #ffffff;"  >
			<div class="itemConsigna">
				<div id="divResGlosa1" name="divResGlosa1" style="font-size:12px;text-align:left;" class="">

					<?php 
					if ($fila_cuestionario_intereses == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_intereses[0]->caracteristica_mayor_2;
					}
					?>
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td class="fondo_azul_1" style="border: solid 1px #ffffff;">
			<div class="itemConsigna"><div id="divResPunto1" name="divResPunto1" style="font-size:28px;text-align:center;" class="">

				<?php 
				if ($fila_cuestionario_intereses == null) {
					echo "-";
				}else{
					echo $fila_cuestionario_intereses[0]->total_intereses_alto_3;
				}
				?>
			</div>
		</div>
	</td>
	<td class="fondo_azul_1" style="border: solid 1px #ffffff;">
		<div class="itemConsigna"><div id="divResInteres1" name="divResInteres1" style="font-size:12px;text-align:left;" class="" >

			<?php 
			if ($fila_cuestionario_intereses == null) {
				echo "-";
			}else{
				echo $fila_cuestionario_intereses[0]->interes_mayor_3;
			}
			?>

		</div>
	</div>
</td>
<td class="fondo_azul_1" style="border: solid 1px #ffffff;"  >
	<div class="itemConsigna">
		<div id="divResGlosa1" name="divResGlosa1" style="font-size:12px;text-align:left;" class="">

			<?php 
			if ($fila_cuestionario_intereses == null) {
				echo "-";
			}else{
				echo $fila_cuestionario_intereses[0]->caracteristica_mayor_3;
			}
			?>

		</div>
	</div>
</td>
</tr>
<tr>

</tr>
<tr>
	<td colspan="3" class="fondoRojoResultado"><div class="itemConsigna">Intereses mínimos</div></td>
</tr>
<tr>
	<td class="fondoPlomoObscuro"><div class="itemConsigna" style="">Puntos</div></td>
	<td class="fondoPlomoClaro"><div class="itemConsigna">Interés</div></td>
	<td class="fondoPlomoObscuro"><div class="itemConsigna" style="text-align:left;">&nbsp;Descripción</div></td>
</tr>
<tr bordercolor="#ffffff">
	<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResPunto3" name="divResPunto3" style="font-size:28px;text-align:center;"  class="" >
		<?php 
		if ($fila_cuestionario_intereses == null) {
			echo "-";
		}else{
			echo $fila_cuestionario_intereses[0]->total_intereses_bajo_1;
		}
		?>

	</div></div></td>
	<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResInteres3" name="divResInteres3" style="font-size:12px;text-align:left;" class="" >

		<?php 
		if ($fila_cuestionario_intereses == null) {
			echo "-";
		}else{
			echo $fila_cuestionario_intereses[0]->interes_menor_1;
		}
		?>

	</div></div></td>
	<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResGlosa3" name="divResGlosa3" style="font-size:12px;text-align:left;" class="">

		<?php 
		if ($fila_cuestionario_intereses == null) {
			echo "-";
		}else{
			echo $fila_cuestionario_intereses[0]->caracteristica_menor_1;
		}
		?>

	</div></div></td>
</tr>
<tr>

</tr>
<tr>
	<td colspan="3" class="fondoBlanco" style="text-align:right;"></td>
</tr>
</tbody></table>
</div>


<br>
<hr>



<p class="texto_azul_2">
	<b> Aptitudes</b>
</p>



<div class="tabla_aptitudes ">	
	<table border="0" cellpadding="10" cellspacing="0" id="TABLA_RES">
		<tbody>
			<tr>
				<td colspan="3"  style="padding-top: 0px;" class="fondoRojoResultado"><div class="itemConsigna">Aptitudes predominantes</div></td>
			</tr>
			<tr>
				<td class="fondoPlomoObscuro"><div class="itemConsigna" style="">Puntos</div></td>
				<td class="fondoPlomoClaro"><div class="itemConsigna">Aptitud</div></td>
				<td class="fondoPlomoObscuro"><div class="itemConsigna" style="text-align:left;">&nbsp;Descripción</div></td>
			</tr>
			<tr>
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResPunto1" name="divResPunto1" style="font-size:28px;text-align:center;" class="">
					<?php 
					if ($fila_cuestionario_aptitudes == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_aptitudes[0]->total_aptitudes_alto_1;
					}
					?>

				</div></div></td>
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResInteres1" name="divResInteres1" style="font-size:12px;text-align:left;" class="" >

					<?php 
					if ($fila_cuestionario_aptitudes == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_aptitudes[0]->aptitud_mayor_1;
					}
					?>

				</div></div></td>
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;"  ><div class="itemConsigna"><div id="divResGlosa1" name="divResGlosa1" style="font-size:12px;text-align:left;" class="">
					<?php 
					if ($fila_cuestionario_aptitudes == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_aptitudes[0]->aptitud_caracteristica_mayor_1;
					}
					?>

				</div></div></td>
			</tr>
			<tr>
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResPunto1" name="divResPunto1" style="font-size:28px;text-align:center;" class="">

					<?php 
					if ($fila_cuestionario_aptitudes == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_aptitudes[0]->total_aptitudes_alto_2;
					}
					?>


				</div></div></td>
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResInteres1" name="divResInteres1" style="font-size:12px;text-align:left;" class="" >

					<?php 
					if ($fila_cuestionario_aptitudes == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_aptitudes[0]->aptitud_mayor_2;
					}
					?>



				</div></div></td>
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;"  ><div class="itemConsigna"><div id="divResGlosa1" name="divResGlosa1" style="font-size:12px;text-align:left;" class="">

					<?php 
					if ($fila_cuestionario_aptitudes == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_aptitudes[0]->aptitud_caracteristica_mayor_2;
					}
					?>



				</div></div></td>
			</tr>
			<tr>
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResPunto1" name="divResPunto1" style="font-size:28px;text-align:center;" class="">

					<?php 
					if ($fila_cuestionario_aptitudes == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_aptitudes[0]->total_aptitudes_alto_3;
					}
					?>



				</div></div></td>
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResInteres1" name="divResInteres1" style="font-size:12px;text-align:left;" class="" >


					<?php 
					if ($fila_cuestionario_aptitudes == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_aptitudes[0]->aptitud_mayor_3;
					}
					?>



				</div></div></td>
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;"  ><div class="itemConsigna"><div id="divResGlosa1" name="divResGlosa1" style="font-size:12px;text-align:left;" class="">

					<?php 
					if ($fila_cuestionario_aptitudes == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_aptitudes[0]->aptitud_caracteristica_mayor_3;
					}
					?>


				</div></div></td>
			</tr>
			<tr>

			</tr>
			<tr>
				<td colspan="3" class="fondoRojoResultado"><div class="itemConsigna">Aptitudes mínimas</div></td>
			</tr>
			<tr>
				<td class="fondoPlomoObscuro"><div class="itemConsigna" style="">Puntos</div></td>
				<td class="fondoPlomoClaro"><div class="itemConsigna">Aptitud</div></td>
				<td class="fondoPlomoObscuro"><div class="itemConsigna" style="text-align:left;">&nbsp;Descripción</div></td>
			</tr>
			<tr bordercolor="#ffffff">
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResPunto3" name="divResPunto3" style="font-size:28px;text-align:center;"  class="" >

					<?php 
					if ($fila_cuestionario_aptitudes == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_aptitudes[0]->total_aptitudes_bajo_1;
					}
					?>

				</div></div></td>
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResInteres3" name="divResInteres3" style="font-size:12px;text-align:left;" class="" >

					<?php 
					if ($fila_cuestionario_aptitudes == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_aptitudes[0]->aptitudes_menor_1;
					}
					?>

				</div></div></td>
				<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResGlosa3" name="divResGlosa3" style="font-size:12px;text-align:left;" class="">

					<?php 
					if ($fila_cuestionario_aptitudes == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_aptitudes[0]->aptitudes_caracteristica_menor_1;
					}
					?>

				</div></div></td>
			</tr>
			<tr>

			</tr>
			<tr>
				<td colspan="3" class="fondoBlanco" style="text-align:right;">


				</td>
			</tr>
		</tbody></table>


	</div>


	<br>
	<hr>
	<p class="texto_azul_2">
		<b> Valores</b>
	</p>


	<div class="tabla_valores">	

		<div class="oculto">			
			<?php 
			if ($fila_cuestionario_valores == null) {
				echo "-";
			}else{
				echo $fila_cuestionario_valores[0]->idCuestionario_valores;
			}
			?>
		</div>


		<table border="0" cellpadding="10" cellspacing="0" id="TABLA_RES">
			<tbody>

				<tr >
					<td colspan="4"  style="padding-top: 0px;" class="fondoRojoResultado"><div class="itemConsigna">Valores predominantes</div></td>
				</tr>
				<tr>
					<td class="fondoPlomoObscuro"><div class="itemConsigna" style="">Puntos</div></td>
					<td class="fondoPlomoClaro"><div class="itemConsigna">Valor</div></td>
					<td class="fondoPlomoObscuro"><div class="itemConsigna" style="text-align:left;">&nbsp;Descripción de la persona que se guía por el valor</div></td>  
					<td class="fondoPlomoObscuro" style="    border: 1px solid;"><div class="itemConsigna" style="text-align:left;">&nbsp;Meta de la persona guiada por este valor</div></td>
				</tr>
				<tr>
					<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResPunto1" name="divResPunto1" style="font-size:28px;text-align:center;" class="total_valores_alto_1">

						<?php 
						if ($fila_cuestionario_valores == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_valores[0]->total_valores_alto_1;
						}
						?>



					</div></div></td>
					<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResInteres1" name="divResInteres1" style="font-size:12px;text-align:left;" class="" >


						<?php 
						if ($fila_cuestionario_valores == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_valores[0]->valor_mayor_1;
						}
						?>
					</div></div></td>
					<td class="fondo_azul_1" style="border: solid 1px #ffffff;"  ><div class="itemConsigna"><div id="divResGlosa1" name="divResGlosa1" style="font-size:12px;text-align:left;" class="">


						<?php 
						if ($fila_cuestionario_valores == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_valores[0]->valor_caracteristica_mayor_1;
						}
						?>
					</div></div></td>  
					<td class="fondo_azul_1" style="border: solid 1px #ffffff;"  ><div class="itemConsigna"><div id="divResGlosa1" name="divResGlosa1" style="font-size:12px;text-align:left;" class="">


						<?php 
						if ($fila_cuestionario_valores == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_valores[0]->valor_meta_mayor_1;
						}
						?>
					</div></div></td>
				</tr>
				<tr>
					<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResPunto1" name="divResPunto1" style="font-size:28px;text-align:center;" class="">


						<?php 
						if ($fila_cuestionario_valores == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_valores[0]->total_valores_alto_2;
						}
						?>
					</div></div></td>
					<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResInteres1" name="divResInteres1" style="font-size:12px;text-align:left;" class="" >


						<?php 
						if ($fila_cuestionario_valores == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_valores[0]->valor_mayor_2;
						}
						?>
					</div></div></td>
					<td class="fondo_azul_1" style="border: solid 1px #ffffff;"  ><div class="itemConsigna"><div id="divResGlosa1" name="divResGlosa1" style="font-size:12px;text-align:left;" class="">

						<?php 
						if ($fila_cuestionario_valores == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_valores[0]->valor_caracteristica_mayor_2;
						}
						?>
					</div></div></td>  
					<td class="fondo_azul_1" style="border: solid 1px #ffffff;"  ><div class="itemConsigna"><div id="divResGlosa1" name="divResGlosa1" style="font-size:12px;text-align:left;" class="">


						<?php 
						if ($fila_cuestionario_valores == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_valores[0]->valor_meta_mayor_2;
						}
						?>
					</div></div></td>
				</tr>

				<tr>

				</tr>
				<tr>
					<td colspan="4" class="fondoRojoResultado"><div class="itemConsigna">Valores mínimos</div></td>
				</tr>
				<tr>
					<td class="fondoPlomoObscuro"><div class="itemConsigna" style="">Puntos</div></td>
					<td class="fondoPlomoClaro"><div class="itemConsigna">Valor</div></td>
					<td class="fondoPlomoObscuro"><div class="itemConsigna" style="text-align:left;">&nbsp;Descripción de la persona que se guía por el valor</div></td>  
					<td class="fondoPlomoObscuro" style="    border: 1px solid;"><div class="itemConsigna" style="text-align:left;">&nbsp;Meta de la persona guiada por este valor</div></td>
				</tr>
				<tr bordercolor="#ffffff">
					<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResPunto3" name="divResPunto3" style="font-size:28px;text-align:center;"  class="" >


						<?php 
						if ($fila_cuestionario_valores == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_valores[0]->total_valores_bajo_1;
						}
						?>
					</div></div></td>
					<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna">

						<div id="divResInteres3" name="divResInteres3" style="font-size:12px;text-align:left;" class="valores_menor_1" >	<?php 
						if ($fila_cuestionario_valores == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_valores[0]->valores_menor_1;
						}
						?>
						

					</div></div></td>
					<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResGlosa3" name="divResGlosa3" style="font-size:12px;text-align:left;" class="">


						<?php 
						if ($fila_cuestionario_valores == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_valores[0]->valores_caracteristica_menor_1;
						}
						?>
					</div></div></td> 
					<td class="fondo_azul_1" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResGlosa3" name="divResGlosa3" style="font-size:12px;text-align:left;" class="">


						<?php 
						if ($fila_cuestionario_valores == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_valores[0]->valores_meta_menor_1;
						}
						?>
					</div></div></td>
				</tr>
				<tr>

				</tr>

			</tbody></table>



		</div>


		<br>
		<hr>
		<p class="texto_azul_2">
			<b> Personalidad</b>
		</p>


		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody>


				<tr>
					<td class="fondoPlomoObscuro" width="250"><div class="itemConsigna" style="padding:10px;">Aspecto</div></td>
					<td class="fondoPlomoClaro"><div class="itemConsigna" style="padding:10px;">Tipos</div></td>
				</tr>
				<tr>
					<td class="fondoRojo" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResPunto1" name="divResPunto1">¿Cómo me relaciono con el mundo?</div></div></td>
					<td class=" fondo_azul_claro"><div class="itemConsigna"><div id="divResInteres1" name="divResInteres1"><table border="0" cellpadding="25" cellspacing="0" width="100%">  <tbody><tr>    <td class="fondo_resultados1" style="border: solid 0px #ffffff;" width="50%" title="Se orienta al mundo externo, a las personas y a las cosas."><span style="font-size:24px;text-align:center;" class="">



						<?php 
						if ($fila_cuestionario_personalidad == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_personalidad[0]->total_extrovertido;
						}
						?>




					</span><br><span style="font-size:12px;text-align:center;">Extrovertido</span></td>    <td class="fondoVerde" style="border: solid 0px #ffffff;" width="50%" title="Se orienta al mundo interno, a sus ideas y sentimientos personales."><span style="font-size:24px;text-align:center;" class="">



						<?php 
						if ($fila_cuestionario_personalidad == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_personalidad[0]->total_introvertido;
						}
						?>


					</span><br><span style="font-size:12px;text-align:center;">Introvertido</span></td>  </tr></tbody></table></div></div></td>


				</tr>
				<tr>
					<td class="fondoRojo" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResPunto2" name="divResPunto2">¿Cómo tomo decisiones?</div></div></td>
					<td class=" fondo_azul_claro"><div class="itemConsigna"><div id="divResInteres2" name="divResInteres2"><table border="0" cellpadding="25" cellspacing="0" width="100%">  <tbody><tr>    <td class="fondo_resultados1" style="border: solid 0px #ffffff;" width="50%" title="Pone énfasis en el resultado de tomar una decisión o resolver un asunto."><span style="font-size:24px;text-align:center;" class="">


						<?php 
						if ($fila_cuestionario_personalidad == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_personalidad[0]->total_juicios;
						}
						?>




					</span><br><span style="font-size:12px;text-align:center;">Basado en los juicios</span></td>    <td class="fondoVerde" style="border: solid 0px #ffffff;" width="50%" title="Le da relevancia al acopio de toda la información necesaria y a la obtención de todos los datos posibles para tomar una decisión."><span style="font-size:24px;text-align:center;" class="">


						<?php 
						if ($fila_cuestionario_personalidad == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_personalidad[0]->total_percepciones;
						}
						?>



					</span><br><span style="font-size:12px;text-align:center;">Basado en las percepciones</span></td>  </tr></tbody></table></div></div></td>
				</tr>
				<tr>
					<td class="fondoRojo" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResPunto3" name="divResPunto3">¿Cómo adquiero información del mundo?</div></div></td>
					<td class=" fondo_azul_claro"><div class="itemConsigna"><div id="divResInteres3" name="divResInteres3"><table border="0" cellpadding="25" cellspacing="0" width="100%">  <tbody><tr>    <td class="fondo_resultados1" style="border: solid 0px #ffffff;" width="50%" title="La da preeminencia a sus percepciones, a los hechos, detalles y eventos concretos."><span style="font-size:24px;text-align:center;" class="">


						<?php 
						if ($fila_cuestionario_personalidad == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_personalidad[0]->total_sensaciones;
						}
						?>



					</span><br><span style="font-size:12px;text-align:center;">Basado en la sensación</span></td>    <td class="fondoVerde" style="border: solid 0px #ffffff;" width="50%" title="Pone el acento en las posibilidades, la imaginación y las totalidades."><span style="font-size:24px;text-align:center;" class="">


						<?php 
						if ($fila_cuestionario_personalidad == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_personalidad[0]->total_intuicion;
						}
						?>



					</span><br><span style="font-size:12px;text-align:center;">Basado en la intuición</span></td>  </tr></tbody></table></div></div></td>
				</tr>
				<tr>
					<td class="fondoRojo" style="border: solid 1px #ffffff;"><div class="itemConsigna"><div id="divResPunto4" name="divResPunto4">¿Cómo juzgo la realidad?</div></div></td>
					<td class=" fondo_azul_claro"><div class="itemConsigna"><div id="divResInteres4" name="divResInteres4"><table border="0" cellpadding="25" cellspacing="0" width="100%">  <tbody><tr>    <td class="fondo_resultados1" style="border: solid 0px #ffffff;" width="50%" title="Se centra en el análisis, usa la lógica y la racionalidad."><span style="font-size:24px;text-align:center;" class="">


						<?php 
						if ($fila_cuestionario_personalidad == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_personalidad[0]->total_pensamiento;
						}
						?>



					</span><br><span style="font-size:12px;text-align:center;">Basado en el pensamiento</span></td>    <td class="fondoVerde" style="border: solid 0px #ffffff;" width="50%" title="Se ocupa de los valores humanos, establece relaciones amistosas y se guía por sus creencias y gustos."><span style="font-size:24px;text-align:center;" class="">


						<?php 
						if ($fila_cuestionario_personalidad == null) {
							echo "-";
						}else{
							echo $fila_cuestionario_personalidad[0]->total_sentimientos;
						}
						?>



					</span><br><span style="font-size:12px;text-align:center;">Basado en los sentimientos</span></td>  </tr></tbody></table></div></div></td>
				</tr>

			</tbody></table>


			<br>
			<hr>
			<p class="texto_azul_2">
				<b>Habilidades Intelectuales</b>
			</p>


			<!-- inicio-->




			<div class="datos_ordenados">
				<div class="habilidad_order   total_naturalista total_numero total_naturalista_numero" data-name="Habilidad naturalista">

					<?php 
					if ($fila_cuestionario_habilidad == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_habilidad[0]->total_naturalista;
					}
					?>


				</div>
				<div class="habilidad_order total_intrapersonal   total_numero total_intrapersonal_numero" data-name="Habilidad intrapersonal">
					<?php 
					if ($fila_cuestionario_habilidad == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_habilidad[0]->total_intrapersonal;
					}
					?>

				</div>
				<div class="habilidad_order total_interpersonal   total_numero total_interpersonal_numero" data-name="Habilidad interpersonal">

					<?php 
					if ($fila_cuestionario_habilidad == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_habilidad[0]->total_interpersonal;
					}
					?>


				</div>
				<div class="habilidad_order total_corporal   total_numero total_corporal_numero" data-name="Habilidad corporal">

					<?php 
					if ($fila_cuestionario_habilidad == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_habilidad[0]->total_corporal;
					}
					?>


				</div>
				<div class="habilidad_order  total_espacial  total_numero total_espacial_numero" data-name="Habilidad espacial">

					<?php 
					if ($fila_cuestionario_habilidad == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_habilidad[0]->total_espacial;
					}
					?>


				</div>
				<div class="habilidad_order total_logico_matematica  total_logico_matematica total_numero total_logico_matematica_numero" data-name="Habilidad lógico-matemática">

					<?php 
					if ($fila_cuestionario_habilidad == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_habilidad[0]->total_logico_matematica;
					}
					?>


				</div>
				<div class="habilidad_order total_musical  total_musical total_numero total_musical_numero" data-name="Habilidad musical">
					
					<?php 
					if ($fila_cuestionario_habilidad == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_habilidad[0]->total_musical;
					}
					?>


				</div>
				<div class="habilidad_order total_linguistica total_linguistica total_numero total_linguistica_numero " data-name="Habilidad lingüística">

					<?php 
					if ($fila_cuestionario_habilidad == null) {
						echo "-";
					}else{
						echo $fila_cuestionario_habilidad[0]->total_linguistica;
					}
					?>


				</div>

			</div>


			<table colspan="0" class="tabla_total_naturalista oculto" cellpadding="0" cellspacing="0" border="0" width="90%" style="width: 91%;">
				<tbody>
					<tr>
						<td style="text-align:left;font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif;color:#ffffff;background-color:#189AA3;cursor:help;" title="Destreza para hacer distinciones en el mundo de la naturaleza y usar este conocimiento de manera productiva. Es importante para biólogos, ingenieros agrónomos, ecologistas, ambientalistas, veterinarios, geólogos." class="total_naturalista">
							<?php 
							if ($fila_cuestionario_habilidad == null) {
								echo "-";
							}else{
								echo $fila_cuestionario_habilidad[0]->total_naturalista;
							}
							?>
						</td>
					</tr>


				</tbody>

			</table>

			<table colspan="0" class="tabla_total_intrapersonal oculto" cellpadding="0" cellspacing="0" border="0" width="90%" style="width: 91%;">
				<tbody>
					<tr>
						<td style="text-align:left;font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif;color:#ffffff;background-color:#189AA3;cursor:help;" title="Capacidad para hacer autoanálisis; implica la destreza para evaluar nuestro proceder, revisar nuestra conducta y nuestros sentimientos más profundos. Es notable en filósofos, psicólogos, sacerdotes, creativos." class="total_intrapersonal">
							<?php 
							if ($fila_cuestionario_habilidad == null) {
								echo "-";
							}else{
								echo $fila_cuestionario_habilidad[0]->total_intrapersonal;
							}
							?></td>
						</tr>


					</tbody>

				</table>

				<table colspan="0" class="tabla_total_interpersonal oculto" cellpadding="0" cellspacing="0" border="0" width="90%" style="width: 91%;">
					<tbody>
						<tr>
							<td style="text-align:left;font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif;color:#ffffff;background-color:#189AA3;cursor:help;" title="Capacidad para trabajar de manera efectiva con la gente y relacionarse con ella demostrando empatía y comprensión. Capacidad para darse cuenta de las motivaciones, necesidades y metas de otras personas. Es muy importante para quienes se dedican a la docencia, ministerios religiosos, relaciones públicas, mercadotecnia o ventas." class="total_interpersonal">

								<?php 
								if ($fila_cuestionario_habilidad == null) {
									echo "-";
								}else{
									echo $fila_cuestionario_habilidad[0]->total_interpersonal;
								}
								?>

							</td>
						</tr>


					</tbody>

				</table>


				<table colspan="0" class="tabla_total_corporal oculto" cellpadding="0" cellspacing="0" border="0" width="90%" style="width: 91%;">
					<tbody>
						<tr>
							<td style="text-align:left;font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif;color:#ffffff;background-color:#189AA3;cursor:help;" title="Capacidad de utilizar el cuerpo con destreza para resolver problemas, crear productos o presentar ideas o emociones. Es importante para atletas, actores, cirujanos, bailarines." class="total_corporal">

								<?php 
								if ($fila_cuestionario_habilidad == null) {
									echo "-";
								}else{
									echo $fila_cuestionario_habilidad[0]->total_corporal;
								}
								?>

							</td>


						</tr>
					</tbody>

				</table>


				<table colspan="0" class="tabla_total_espacial oculto" cellpadding="0" cellspacing="0" border="0" width="90%" style="width: 91%;">
					<tbody>
						<tr>
							<td style="text-align:left;font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif;color:#ffffff;background-color:#189AA3;cursor:help;" title="Capacidad para pensar en imágenes, visualizando un resultado futuro. Se manifiesta por una gran capacidad imaginativa, orientación espacial y destreza para representar la realidad gráficamente o por medio de dibujos. Es muy notable en arquitectos, pintores, escultores, marinos, fotógrafos, cineastas y planeadores estratégicos." class="total_espacial">

								<?php 
								if ($fila_cuestionario_habilidad == null) {
									echo "-";
								}else{
									echo $fila_cuestionario_habilidad[0]->total_espacial;
								}
								?>

							</td>


						</tr>
					</tbody> 
				</table>


				<table colspan="0" class="tabla_total_logico_matematical oculto" cellpadding="0" cellspacing="0" border="0" width="90%" style="width: 91%;">
					<tbody>
						<tr>
							<td style="text-align:left;font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif;color:#ffffff;background-color:#189AA3;cursor:help;" title="Capacidad para razonar y calcular, así como pensar de manera lógica y sistemática. Es importante para ingenieros, científicos, economistas, actuarios, contadores, detectives, administradores." class="total_logico_matematica">

								<?php 
								if ($fila_cuestionario_habilidad == null) {
									echo "-";
								}else{
									echo $fila_cuestionario_habilidad[0]->total_logico_matematica;
								}
								?>

							</td>


						</tr>
					</tbody> 
				</table>


				<table colspan="0" class="tabla_total_musical oculto" cellpadding="0" cellspacing="0" border="0" width="90%" style="width: 91%;">
					<tbody>
						<tr>
							<td style="text-align:left;font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif;color:#ffffff;background-color:#189AA3;cursor:help;" title="Capacidad para cantar bien, ejecutar instrumentos musicales, componer, comprender y apreciar la música. La tienen músicos, cantantes, compositores e ingenieros de sonido." class="total_musical">

								<?php 
								if ($fila_cuestionario_habilidad == null) {
									echo "-";
								}else{
									echo $fila_cuestionario_habilidad[0]->total_musical;
								}
								?>

							</td>


						</tr>
					</tbody>
				</table>


				<table colspan="0" class="tabla_linguistica oculto" cellpadding="0" cellspacing="0" border="0" width="90%" style="width: 91%;">
					<tbody>
						<tr>
							<td style="text-align:left;font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif;color:#ffffff;background-color:#189AA3;cursor:help;" title="Capacidad para leer, escribir y comunicarse con palabras. La presentan escritores, periodistas, poetas, oradores y comediantes." class="total_linguistica">

								<?php 
								if ($fila_cuestionario_habilidad == null) {
									echo "-";
								}else{
									echo $fila_cuestionario_habilidad[0]->total_linguistica;
								}
								?>


							</td>


						</tr>
					</tbody>
				</table>





				<!-- fin-->


				<hr>



				<br>
				<br>


				<div class="mensaje_final"></div>

				<br>
				<div class="panel_final ">
					<p>De acuerdo con los resultados anteriores, tu perfil es <b><span class="porcentaje"></span></b> afín con la siguiente área:</p>
					<table>
						<tbody>
							<tr> 
								<td class="fondoPlomoObscuro">Intereses y aptitudes </td>
								<td class="fondoPlomoClaro">Valores</td>
								<td class="fondoPlomoObscuro">Rasgos de personalidad afines</td>
								<td class="fondoPlomoClaro">Habilidades intelectuales</td>
							</tr>
							<tr> 
								<td class="fondo_azul_1 espacio_celda">
									<span class="interes_1_final"></span><br>
									<span class="interes_2_final"></span><br>
									<span class="interes_3_final"></span> 
								</td>
								<td class="fondo_azul_1 espacio_celda">
									<span class="valor_1_final"></span> <br>
									<span class="valor_2_final"></span> <br>
									<span class="valor_3_final"></span> 
								</td>
								<td class="fondo_azul_1 espacio_celda">
									<span class="personalidad_1_final"></span> <br>
									<span class="personalidad_2_final"></span> <br>
									<span class="personalidad_3_final"></span>  
								</td>
								<td class="fondo_azul_1 espacio_celda">
									<span class="habilidad_1_final"></span> <br>
									<span class="habilidad_2_final"></span> <br>
									<span class="habilidad_3_final"></span> 
								</td>

							</tr>
						</tbody>
					</table>
					<br>
					<br>
					<table>
						<tbody>
							<tr> 
								<td class="fondoPlomoObscuro">Áreas </td>
								<td class="fondoPlomoClaro" style="width: 347px;">Objetivos del área</td>
								<td class="fondoPlomoObscuro">Algunas carreras</td>
							</tr>
							<tr> 
								<td class="fondo_azul_1 espacio_celda">
									<p class="areas_final"></p> 
								</td>
								<td class="fondo_azul_1 espacio_celda">
									<p class="objetivos_final"></p>  
								</td>
								<td class="fondo_azul_1 espacio_celda">
									<p class="carreras_final"></p>  
								</td> 

							</tr>
						</tbody>
					</table>
					<br>
					<br>



				</div>




			</div>


			<div class="row " style="border: 2px solid #d2db0d;"> 			
				<div class="input-field col s8" style="font-size: 34px; ">
					Puedes imprimir tus resultados 
				</div>		
				<div class="input-field col s4" style="">
					<div class="center fondo_verde imprimir_resultados" onclick="javascript:imprSelec('contenido_imprimir')"  style=" float: right;width: 200px; cursor:pointer;font-size: 25px;     margin-right: 22px;">
						<span class="">Imprimir</span>
					</div>
				</div>
			</div>



			<div class="row " style="border: 2px solid #d2db0d;"> 			
				<div class="input-field col s8" style="">						
					<input style="height: 91px;    font-size: 34px;    width: 100%;" placeholder="Escribe el correo de tu orientador" id="correo_envio" type="text" class="validate">
				</div>		
				<div class="input-field col s4" style="">						
					<div class="center fondo_verde enviar_correo" style=" float: right;width: 200px; cursor:pointer;font-size: 25px;    margin-top: 15px;    margin-right: 22px;">
						<span class="">Enviar</span>
					</div>
				</div>
			</div>





		</div>
	</div>


</body>
</html>