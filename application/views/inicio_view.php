<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>PEARSON</title>

	<style type="text/css">
		body{
			    background: #9a9a9a; 
	 font-family: 'Montserrat', sans-serif;

		}
		
		.container{
			    background: white;
				border: 0px solid;
				max-width: 700px !important;
				padding: 50px;
				margin-top: 34px !important;
				margin-bottom: 34px !important;
		}
		
		  .fondoAzul{
				background: #0080a4;
				color: #fff;
				padding: 10px;
		  }
		  
		  .dropdown-content li>a, .dropdown-content li>span{
			      color: #0080a4 !important;
		  }
		  
		 input:not([type]):focus:not([readonly]), input[type=text]:not(.browser-default):focus:not([readonly]), input[type=password]:not(.browser-default):focus:not([readonly]), input[type=email]:not(.browser-default):focus:not([readonly]), input[type=url]:not(.browser-default):focus:not([readonly]), input[type=time]:not(.browser-default):focus:not([readonly]), input[type=date]:not(.browser-default):focus:not([readonly]), input[type=datetime]:not(.browser-default):focus:not([readonly]), input[type=datetime-local]:not(.browser-default):focus:not([readonly]), input[type=tel]:not(.browser-default):focus:not([readonly]), input[type=number]:not(.browser-default):focus:not([readonly]), input[type=search]:not(.browser-default):focus:not([readonly]), textarea.materialize-textarea:focus:not([readonly]){
			  border-bottom: 1px solid #0080a4 !important;
    -webkit-box-shadow: 0 1px 0 0 #0080a4 !important;
    box-shadow: 0 1px 0 0 #0080a4 !important;
			  
		  }
		  
		  input:not([type]):focus:not([readonly])+label, input[type=text]:not(.browser-default):focus:not([readonly])+label, input[type=password]:not(.browser-default):focus:not([readonly])+label, input[type=email]:not(.browser-default):focus:not([readonly])+label, input[type=url]:not(.browser-default):focus:not([readonly])+label, input[type=time]:not(.browser-default):focus:not([readonly])+label, input[type=date]:not(.browser-default):focus:not([readonly])+label, input[type=datetime]:not(.browser-default):focus:not([readonly])+label, input[type=datetime-local]:not(.browser-default):focus:not([readonly])+label, input[type=tel]:not(.browser-default):focus:not([readonly])+label, input[type=number]:not(.browser-default):focus:not([readonly])+label, input[type=search]:not(.browser-default):focus:not([readonly])+label, textarea.materialize-textarea:focus:not([readonly])+label{
			  
			  			      color: #0080a4 !important;

		  }
		  
		  .btn, .btn-large, .btn-small{
			  background-color: #0080a4  !important;
		  }
		  
		  .oculto{
			  display: none ;
		  }
		  
		  #codigo {
			text-transform: uppercase;
		}
		  
	</style>
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


	  <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="<?=base_url();?>media/js/calculos_finales.js"></script>

    <script src="<?=base_url();?>media/js/funciones.js"></script>
    <script src="<?=base_url();?>media/js/jquery.mask.js"></script>
    <script> 
	</script>
</head>
<body>


	<div class="container" >


		 <div class="row" style="">
		 			<img src="<?=base_url();?>media/images/logo.png" class="responsive-img  " style=" max-width: 229px;margin:auto;" >
		 </div>
		 
		 <div class="row pantalla1" style="">
			<h4>Ingresa tu código</h4>
			
			<div class="input-field col s12">
				  <input id="id_codigo_actual" type="hidden" class="">
				  <input id="codigo" type="text" class="validate" placeholder="XXXXX-XXXXX-XXXXX-XXXXX-XXXXX-XXXXX">
				  <label for="codigo">Código</label>
			</div>
			
		  <div class="row ">	
			   <div class="right btn waves-effect waves-light comprobarDisponibilidad">Comprobar Disponibilidad
					<i class="material-icons right">send</i>
			  </div> 

		  </div>
		  <br>
		  
		  
		    <div class=" cargador oculto">
				<div class="progress">
					  <div class="indeterminate "></div>
				</div>
			</div>
		   <div class="row  center mensaje1 oculto">	
				 
				<h5>¡Perfecto! el código esta disponible, ahora procede a realizar tu registro.</h5>
				
				
				<img src="<?=base_url();?>media/images/ok.svg" class="responsive-img  " style=" max-width: 100px;margin:auto;" >
		  </div>
		  
		  <div class="row  center mensaje_usado oculto">	
				 
				<h5>¡Vaya! al parecer el código ya se utilizó, ingresa otro.</h5>
				
				
				<img src="<?=base_url();?>media/images/mal.svg" class="responsive-img  " style=" max-width: 100px;margin:auto;" >
		  </div>
		  
			
			  
			  
		   <div class="row  center mensaje2 oculto">
					
				<h5>¡Vaya! al parecer el código no es válido, ingrésalo nuevamente.</h5>
				<img src="<?=base_url();?>media/images/mal.svg" class="responsive-img  " style=" max-width: 100px;margin:auto;" >
				
		  </div>

		 </div>
			
			
		

		<div class="row pantalla2 oculto" style="">
			
		 
			<form class="col s12">
			  
			  <div class="formulario">
			  
			  	<h4>Formulario de Pre-Validación</h4>
				<h6>Tu código: <span class="codigo_confirmacion"></span></h6>
		 
					  <div class="row">
						<h5 class="fondoAzul">Datos del Estudiante:*</h5>

						<div class="input-field col s6">
						  <input id="dni" type="text" class="validate" maxlength="8" onkeypress='validate(event)'>
						  <label for="dni">DNI</label>
						</div>
						<div class="input-field col s6">
						  <input id="correo" type="email" class="validate" onkeypress='validarCorreo()'>
						  <label for="correo">Correo electronico</label>
						  <div id='result'></div>

						</div>
					  </div>
					 
					  <div class="row">
						<div class="input-field col s6">
						  <input id="nombre" type="text" class="validate">
						  <label for="nombre">Nombre</label>
						</div>
						<div class="input-field col s6">
						  <input id="apellido" type="text" class="validate">
						  <label for="apellido">Apellido</label>
						</div>
					  </div>
					  
					   
					<div class="row">
						<h5 class="fondoAzul">Datos de la Institución:*</h5>

						  <div class="input-field col s12">
							<select id="sede">
							  <option value="" disabled selected>Selecciona</option>
								<option value="Arequipa - Bustamante y Rivero">Arequipa - Bustamante y Rivero</option>
								<option value="Arequipa 2 - Cerro Colorado">Arequipa 2 - Cerro Colorado</option>
								<option value="Arequipa 3 - Sachaca">Arequipa 3 - Sachaca</option>
								<option value="Ate 1 - Santa Clara">Ate 1 - Santa Clara</option>
								<option value="Ate 2 - Vitarte">Ate 2 - Vitarte</option>
								<option value="Ate 3 - Puruchuco">Ate 3 - Puruchuco</option>
								<option value="Ate 4 - Santa María">Ate 4 - Santa María</option>
								<option value="Callao 1 - Saenz Peña">Callao 1 - Saenz Peña</option>
								<option value="Callao 2 - Lemos">Callao 2 - Lemos</option>
								<option value="Carabayllo 1 - Enace">Carabayllo 1 - Enace</option>
								<option value="Carabayllo 2 - San Antonio">Carabayllo 2 - San Antonio</option>
								<option value="Cercado - Sánchez Pinillos">Cercado - Sánchez Pinillos</option>
								<option value="Chaclacayo">Chaclacayo</option>
								<option value="Chiclayo">Chiclayo</option>
								<option value="Chimbote">Chimbote</option>
								<option value="Chincha">Chincha</option>
								<option value="Chorrillos 1 - Villa">Chorrillos 1 - Villa</option>
								<option value="Chorrillo 2 - Horizontes">Chorrillo 2 - Horizontes</option>
								<option value="Comas 1 - Condorcanqui">Comas 1 - Condorcanqui</option>
								<option value="Comas 2 - El Retablo">Comas 2 - El Retablo</option>
								<option value="Cusco - Larapa">Cusco - Larapa</option>
								<option value="Huacho">Huacho</option>
								<option value="Huancayo">Huancayo</option>
								<option value="Ica">Ica</option>
								<option value="Los Olivos 1 - Villa Sol">Los Olivos 1 - Villa Sol</option>
								<option value="Los Olivos 2 - Santa Ana">Los Olivos 2 - Santa Ana</option>
								<option value="Piura 1 - Los Ejidos">Piura 1 - Los Ejidos</option>
								<option value="Pueblo Libre - Bertello">Pueblo Libre - Bertello</option>
								<option value="Rimac 1 - Santo Toribio">Rimac 1 - Santo Toribio</option>
								<option value="Rimac 2 - Sporting Cristal">Rimac 2 - Sporting Cristal</option>
								<option value="San Miguel 1">San Miguel 1</option>
								<option value="San Miguel 2">San Miguel 2</option>
								<option value="San Miguel - La Paz 1">San Miguel - La Paz 1</option>
								<option value="San Miguel - La Paz 2">San Miguel - La Paz 2</option>
								<option value="SJL 1 - Campoy">SJL 1 - Campoy</option>
								<option value="SJL 2 - Canto Grande">SJL 2 - Canto Grande</option>
								<option value="SJL 3 - Arabiscos">SJL 3 - Arabiscos</option>
								<option value="SJL 4 - El Sol">SJL 4 - El Sol</option>
								<option value="SMP 1 - Perú">SMP 1 - Perú</option>
								<option value="SMP 2 - Canta Callao">SMP 2 - Canta Callao</option>
								<option value="Surco 1 - Los Faisanes">Surco 1 - Los Faisanes</option>
								<option value="Surco 2 - La Campiña">Surco 2 - La Campiña</option>
								<option value="Surco 3 - Universo">Surco 3 - Universo</option>
								<option value="Surco 4 - Ambrosio">Surco 4 - Ambrosio</option>
								<option value="Tacna">Tacna</option>
								<option value="Trujillo 1 - El Golf">Trujillo 1 - El Golf</option>
								<option value="Trujillo 2 - Santa">Trujillo 2 - Santa</option>
								<option value="Villa El Salvador - Las Laderas de Villa">Villa El Salvador - Las Laderas de Villa</option>

							</select>
							<label>Sede</label>
						  </div>
						  <div class="input-field col s12">
							<select id="grado">
							  <option value="" disabled selected>Selecciona</option>
							  <option value="4">4°</option>
								<option value="5">5°</option>
								<option value="6">6°</option>
								<option value="7">7°</option>
								<option value="8">8°</option>
								<option value="9">9°</option>
								<option value="10">10°</option>
								<option value="11">11°</option>

							</select>
							<label>Grado</label>
						  </div>
					  </div>
					  
					  
					  <div class="row">
						<h5 class="fondoAzul">Comprobante de Pago:*</h5>

						<div class="input-field col s6">
						  <p>
						  <label>
							<input name="comprobante1" type="radio"  id="boleta"/>
							<span>Tipo Boleta</span>
						  </label>
						</p>
						</div> 
						<div class="input-field col s6">
						  <p>
						  <label>
							<input name="comprobante1" type="radio"  id="factura"/>
							<span>Tipo Factura</span>
						  </label>
						</p>
						</div> 
					  </div>
					  
					  
						<div class="row">
							<div class="input-field col s7">
							  <input id="comprobante" type="text" class="validate" placeholder="" >
							  <label for="comprobante">Indicar N° de comprobante de pago:</label>
							</div>	
							<div class="input-field col s5">
								<img src="<?=base_url();?>media/images/image2.png" class="responsive-img  " style=" max-width: 277px;margin:auto;" >

							</div>				 
						</div>
					  
					  <div class="row">	
						<h5 class="fondoAzul">Producto adquirido</h5>
						<h5 class="producto">Selecciona el grado</h5> 
					  </div>
					  
					  <div class="row ">	
						   <div class="right btn waves-effect waves-light enviarResultados"    >Registrar
								<i class="material-icons right">send</i>
						  </div> 
					  </div>
					  
					   <div class=" cargador oculto">
				<div class="progress">
					  <div class="indeterminate "></div>
				</div>
			</div>
				</div>
			  
			  
				  
			   <div class="row  center mensaje3 oculto">	
					<h5>Muchas gracias, has sido registrado correctamente.</h5>
					<img src="<?=base_url();?>media/images/ok.svg" class="responsive-img  " style=" max-width: 100px;margin:auto;" >
			  </div>
			   <div class="row  center mensaje4 oculto">	
					<h5>¡Vaya! al parecer no te pudimos registrar, inténtalo nuevamente.</h5>
					<img src="<?=base_url();?>media/images/mal.svg" class="responsive-img  " style=" max-width: 100px;margin:auto;" >
				
			  </div>
				  
				  
			  
			  
			 
			</form>
		  </div>
	</div>
		  

</body>
</html>