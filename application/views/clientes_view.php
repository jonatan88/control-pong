<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
	<title>SUA</title>

	<style type="text/css">

</style>
<?=$assets;?>
<script> 
	var save_method; 
	var table;
	$(document).ready(function($) {




		table = $('#tabla').DataTable({ 
			"language":{
				"decimal":        "",
				"emptyTable":     "Sin datos",
				"info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
				"infoEmpty":      "Mostrando 0 a 0 de 0 registros",
				"infoFiltered":   "( _MAX_ registros filtrados)",
				"infoPostFix":    "",
				"thousands":      ",",
				"lengthMenu":     " ",
				"loadingRecords": "Cargando...",
				"processing":     "Procesando...",
				"search":         "Buscar:",
				"zeroRecords":    "No se han encontrado registros.",
				"paginate": {
					"first":      "Primero",
					"last":       "Último",
					"next":       "Siguiente",
					"previous":   "Anterior"
				},
				"aria": {
					"sortAscending":  ": activate to sort column ascending",
					"sortDescending": ": activate to sort column descending"
				}
			},

			"processing": true, 
			"serverSide": true,  
			"order": [],  

			"ajax": {
				"url": "<?php echo site_url('Clientes/obtener_datos') ?>",
				"type": "POST" 
			},

			"columnDefs": [
			{ 
				"targets": [ 0 ],  
				"orderable": false,  
			},
			],

		});

		$('.enviar_cliente').on('click', function () {
			save();
		});


		$('.modal-trigger').click(function(event) {
			$('form')[0].reset();  
		});
	});	 	


	function save() {
		var url;

		$('.mensaje_ok,.mensaje_mal').hide();

		idClientes = $.trim($('#idClientes').val());
		cliente = $.trim($('#cliente').val());
		contacto = $.trim($('#contacto').val());
		cli_estatus = $.trim($('#cli_estatus').val());


		if (save_method == 'add') {
			url = "<?php echo site_url('Clientes/enviarDatos') ?>";
			
		} else {

			url = "<?php echo site_url('Clientes/actualizarDatos') ?>";
			
		}
		



		if(cliente == "" || contacto == "" || cli_estatus == ""){
			M.toast({html: 'Completa los datos.'});
		}else{

			jQuery.ajax({
				type: "POST",
				url: url,
				data: $('#form').serialize(),
				dataType: "JSON",

				success: function(data) {	
					//console.log(data);				 						

					if (save_method == 'add') {
						M.toast({html: 'Guardado correctamente.'});
					} else {

						M.toast({html: 'Actualizado correctamente.'});

					}

					table.ajax.reload(); 


					$('.modal').modal('close');
					$('input').val(""); 
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					M.toast({html: 'Error al guardar.'});
					M.toast({html: textStatus});
					M.toast({html: errorThrown});

					console.log('Error');
					$('.mensaje_mal').slideDown();
					$('.progress').hide();
				}
			}); 

		}

	}


	function agregar() {
		save_method = 'add';
		$('form')[0].reset(); 
		$('.modal').modal('open'); 
		$('.enviar_cliente').text('Guardar');  

	}

	function editar(id) {

		save_method = 'update';
		$('.enviar_cliente').text('Actualizar');  

		$('form')[0].reset();  
		$.ajax({
			url: "<?php echo site_url('Clientes/ajax_edit/') ?>"+ id,
			type: "POST",
			dataType: "JSON",
			success: function(data) {

				$('label').attr({
					class: 'active'
				});

				$('[name="idClientes"]').val(data.idClientes);
				$('[name="cliente"]').val(data.cliente);
				$('[name="contacto"]').val(data.contacto);
				$('[name="cli_estatus"]').val(data.cli_estatus);


				$('select').formSelect();


				$('.modal').modal('open');  
				$('.modal-title').text('Editar'); 
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert('Error al obtener datos.');
			}
		});
	}





</script>
</head>
<body>


	<div class="container">

		<?=$menu;?>
		
		<h4>Clientes</h4>
		<br>

		<a class="btn-floating btn-large  waves-light red fixed-action-btn" onclick="agregar();"><i class="material-icons">add</i></a>

		<!-- Modal -->
		<div id="modal1" class="modal">
			<div class="modal-content">


				<div class="row" style="    width: 500px;    margin: auto;">


					<form class="" id="form">
						<input type="hidden" value="" id="idClientes"  name="idClientes" />

						<div class="row">
							<div class="input-field col s12 l12 ">
								<h4>Registro de clientes</h4>
							</div>					
						</div> 

						<div class="row">
							<div class="input-field col s12 l12 ">
								<input placeholder="" id="cliente" name="cliente" type="text" class="validate">
								<label for="Cliente">Cliente</label>
							</div>					
						</div> 

						<div class="row">
							<div class="input-field col s12 l12 ">
								<input placeholder="" id="contacto" name="contacto" type="text" class="validate">
								<label for="Contacto">Contacto</label>
							</div>					
						</div> 

						<div class="row">
							<div class="input-field col s12 l12 ">
								<select id="cli_estatus" name="cli_estatus">
									<option value="" disabled selected>Selecciona...</option>
									<option value="1">Activo</option> 
									<option value="0">Inactivo</option> 
								</select>
								<label for="cli_estatus">Estatus</label>
							</div>					
						</div> 

					</form>
				</div>

			</div>
			<div class="modal-footer">

				<button class="btn waves-effect waves-light enviar_cliente" type="submit" name="action">Guardar
					<i class="material-icons right">send</i>
				</button>

			</div>
		</div>




		<table id="tabla" class="display  ">
			<thead>
				<tr>
					<th>ID</th>
					<th>Cliente</th>
					<th>Contacto</th> 
					<th>Estado</th> 
					<th>Fecha Alta</th> 
					<th>Editar</th> 
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>

	</div>



</body>
</html>