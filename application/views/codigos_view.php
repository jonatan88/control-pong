﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
﻿<!DOCTYPE html>
<html lang="es">
<head>
 		 			<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="robots" content="noindex, nofollow">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
  
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://132.248.252.241/practicas/Asma/media/js/html5shiv.js"></script>
	<![endif]-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

 

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>

	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js">	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js">	</script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js">	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js">	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js">	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js">	</script>


          <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
     <script src="<?=base_url();?>media/js/jquery.ui.touch-punch.min.js"></script>

     
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>	<title>	Calificaciones</title>
	<style type="text/css">
	
	table{
		font-size: 14px !important;
	}
	</style>
</head>

<body>
	<?php echo $menu; ?><div class="container">
	 
 		 
		<br />
 		
		<!--
			<button class="waves-effect waves-light btn modal-trigger" data-target="modal1"  onclick="add_person()"  ><i class="material-icons">add</i>Nuevo</button>
		-->

		  
		<br />
		<br />
		<table id="table" class="table table-striped table-bordered highlight" >
		  <thead>
			<tr>

				
					<th>ID</th>

					<th>Código</th>

					<th>Utilizado</th>
 
 			  <th>Alta en sistema</th>
			  				<!--<th style="width:154px;">Editar</th>-->

			</tr>
		  </thead>
		  <tbody>
		  </tbody>
		  
    </table>
		 
	 <script type="text/javascript">
      var save_method; //for save method string
    var table;
    $(document).ready(function() {
	 


      table = $('#table').DataTable({
        "Procesando": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
			"url" : "<?php echo site_url('Codigos/ajax_list')?>",
            "type": "POST"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        {
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],
      });
    });
  function add_person()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal1').modal('open'); // show bootstrap modal
      $('.modal-title').text('Agregar'); // Set Title to Bootstrap modal title
    }
    function edit_person(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
      //Ajax Load data from ajax
      $.ajax({
 		url : "<?php echo site_url('Codigos/ajax_edit/')?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
			
		
			
		$('[name="idCodigos"]').val(data.idCodigos);
		$('[name="codigo"]').val(data.codigo);
		$('[name="utilizado"]').val(data.utilizado); 
 
			
            $('#modal1').modal('open');  
            $('.modal-title').text('Editar'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error al obtener datos.');
        }
    });
    }
    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }
    function save()
    {
      var url;
      if(save_method == 'add') 
      {
          url = "<?php echo site_url('Codigos/ajax_add')?>";
      }
      else
      {
         
		url = "<?php echo site_url('Codigos/ajax_update')?>";

      }
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal1').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error agregando / editando datos.');
            }
        });
    }
    function delete_person(id)
    {
      if(confirm('¿Esta seguro de eliminarlo?'))
      {
        // ajax delete data to database
          $.ajax({
 			
			url : "<?php echo site_url('Alumnos/ajax_delete/')?>"+id,

					
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal1').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
      }
    }
  </script>
  <!--  modal -->
  
   <div id="modal1" class="modal ">
    <div class="modal-content">
 		 
		  <div class="row">
			<form  action="#"  id="form"  class="col s12">
			 
<!-- Form Name -->
<legend>Nuevo</legend>
  
  	<input type="hidden" value="" name="idCodigos"/>
<!-- Text input-->
					<div class="input-field col l12 s12">
						<input id="codigo" type="text" class="validate">
						<label for="dni">Código
</label>
					</div><!-- Text input-->
					<div class="input-field col l12 s12">
						<input id="utilizado" type="text" class="validate">
						<label for="utilizado">Utilizado
</label>
					</div>
					
 
		
 
 
</form>
          </div>
          <div class="modal-footer">
  			
				<div class="row">
					<div class="  col l6 s6 center">
						<a class="waves-effect waves-light btn modal-close">Cerrar</a>
					</div>
					<div class="  col l6 s6 center">
						<a class="waves-effect waves-light btn" id="btnSave" onclick="save()" >Guardar</a>
					</div>
				</div>
			
          </div>
        </div><!-- /.modal-content -->
	 

</div>  <!-- End Bootstrap modal -->
</div>
		 
	 	</body>
 </html>