<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
 	<title>SUA</title>

	<style type="text/css">
	 
	</style>
 	<?=$assets;?>
     
    <script> 
	</script>
</head>
<body style="background: #606060;">

 	<div class="container" style="height: 609px !important;">
			
 
		 <div class="row pantalla1" style="margin-top: 0px;">
 			
			 
			 	
			<div class="   " style=" ">			
 					
					<div class="row pantalla_login m12 " style="">
					
 							<form >
 							<div class="row" style="    margin-bottom: 0px;">
									<h5 class="fondoAzul" style="margin-left: -31px;    margin-right: -32px;    margin-top: -47px;">Iniciar Sesión</h5>
 
									 <div class="input-field col m1">
									  <i class="material-icons" style="color: #818FA2;">person</i>

 									</div> 
									 <div class="input-field col m11">
									  <input id="correo_login" type="email" class="validate" maxlength="50"  onkeypress='validarCorreoLogin()' placeholder="Correo electronico:">
 									</div> 
							</div>
							 
							<div class="row">  
										 
									<div class="input-field col m1">
										<i class="material-icons" style="color: #818FA2;">lock</i>

 									</div> 
						 
									  
									  
									<div class="input-field col m11">
									  <input id="password_login" type="password" class="validate" maxlength="50" placeholder="Contraseña:">
 									</div>
									
															
										<div class="input-field col m12">
 
												 <div class="input-field col m6">
													<div class="row">  
														 <div class="input-field col m3">
																<img src="<?=base_url();?>media/images/signo.svg" class="" style=" max-width: 16px;" > 
														</div> 
	
														<div class="input-field col m9">
															<div class="boton_recuperar" style="color: #898989; cursor:pointer;    margin-left: -15px;    z-index: 99; text-align: center;   font-size: 11px;   ">Olvidé mi contraseña</div>
														 
														</div> 
													</div> 
												</div> 
												 <div class="input-field col m6">
														<img src="<?=base_url();?>media/images/boton_iniciar.svg" type="submit" class="responsive-img  mano boton_inicio_sesion" style=" max-width: 151px;   " >
														<div id='result_login2' style="color:red;"></div>
												</div> 
											
 
												
												
												<br>
												
										</div>
										
								
							</div>
							<div class="row">
										<div class="input-field col m6">
										  
										</div>	
							</div>
						</form>
					</div>
					
					<div class=" cargador oculto">
						<div class="progress">
							  <div class="indeterminate "></div>
						</div>
					</div>
					
				<div class="row  center mensaje_ok oculto">	
					<h5>Muchas gracias, has sido registrado correctamente, ahora inicia sesión.</h5>
					<img src="<?=base_url();?>media/images/ok.svg" class="responsive-img  " style=" max-width: 100px;margin:auto;" >
			  </div>
			   <div class="row  center mensaje_mal oculto">	
					<h5>¡Vaya! al parecer no te pudimos registrar, inténtalo nuevamente.</h5>
 				
			  </div>
			  
			  
			  <div class="row pantalla_recuperar oculto" style="">
					
 							<form >
 							<div class="row">
							<h5 class="fondoAzul" style="margin-left: -19px;    margin-right: -20px;    margin-top: -47px;">Recuperar contraseña</</h5>

 								<div class="input-field col m6">
								  
								</div>
							</div>
							 
							<div class="row">  
									  
									<div class="input-field col m7">
									  <input id="email_recuperar" type="email" class="validate" maxlength="80"  onkeypress='validarCorreoRecuperar()' placeholder="Correo electronico:">
 									</div> 
									
															
										<div class="input-field col m5">


												<img src="<?=base_url();?>media/images/boton_recuperar.svg" type="submit" class="responsive-img  mano boton_recuperar_enviar" style=" max-width: 151px;    " >
										</div>
										
								
							</div>
							<div class="row">
										<div class="input-field col m6">
										  
										</div>	
							</div>
						</form>
					</div>
					
			  		
			  
			</div>
			
			
		</div>
		
	
			
		
 
	</div>
		  

</body>
</html>