<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Codigos_model extends CI_Model {

	var $table = 'codigos';
	
	
	
	
	var $column = array(
		
	'idCodigos',
	'codigo',
	'utilizado',
	'codigos_tp');
		

	var $order = array('tp' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	/*
	//Template para llenar ListaDropDown
	function ListaDropDown(){
			$this->db->select("idproyecto, nombreProyecto");
			$this->db->from("proyecto");
		 		
			
			$query = $this->db->get();
			
			if($query->num_rows() > 0){
				$query = $query->result_array();
				foreach($query AS $valor){
					$resultados[$valor['idproyecto']] =   $valor['nombreProyecto'];
				}
				return $resultados;
			}else{
				return false;
			}
			
		}*/
		
		
		

			/*
			//template para traer por ID 
				function traePorID($id){
			$this->db->select("*");
			$this->db->where('idproyecto',$id);

			$query = $this->db->get('proyecto');

			if($query->num_rows()>0){

				return $query->result_array();

			}else{

				return -1;

			}

		}
		
		*/

	private function _get_datatables_query()
	{
		
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('idCodigos',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data); 
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('idCodigos', $id);
		$this->db->delete($this->table);
	}


}
?>