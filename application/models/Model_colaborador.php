<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_colaborador extends CI_Model{

	var $table = 'usuarios';
	var $column_order = array(null, 'nombre','correo'); 
	var $column_search = array('nombre','correo'); 
	var $order = array('idUsuarios' => 'asc');

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('email');
		$this->load->helper(array('url'));


	}


	public function traer_datos()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

 

	private function _get_datatables_query()
	{


		$this->db->from($this->table);
		$i = 0;

		foreach ($this->column_search as $item)  
		{
			if($_POST['search']['value']) 
			{

				if($i===0) 
				{
					$this->db->group_start();  
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)  
					$this->db->group_end();  
			}
			$i++;
		}

		if(isset($_POST['order'])) 
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}



	public function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id) {
		$this->db->from($this->table);
		$this->db->where('idUsuarios',$id);
		$query = $this->db->get();

		return $query->row();
	}


	private function _insertar_bitacora($data, $tipo)
	{

		switch ($data['us_estatus']) {
			case '1':
			$estatus = "Activo";				
			break;
			case '0':
			$estatus = "Inactivo";				
			break;			 
		}		
		$this->db->set('accion', $tipo . ' de Colaborador: '. $data['nombre'] . ', estatus: ' . $estatus);
		$this->db->set('usuario', $this->session->userdata("nombre"));
		$this->db->insert('bitacora'); 
	}

	function insertar($data){
		$this->db->insert($this->table,$data);
		$this->_insertar_bitacora($data, 'Alta');
		return $this->db->insert_id();

	}

	public function update($where, $data) {
		$this->db->update($this->table, $data, $where);
		$this->_insertar_bitacora($data, 'Actualización'); 
		return $this->db->affected_rows();
	}

 
 

}




?>