<?php 

defined('BASEPATH') OR exit('No direct script access allowed');



class Model_home extends CI_Model{





	public function __construct() {

		parent::__construct();

		$this->load->database();

		$this->load->library('email');

		$this->load->helper(array('url'));





	}





	function traer_usuarios_actividades()

	{

		$this->db->distinct();



		$this->db->select('nombre,observaciones, horas,fecha,fecha_fin,color_usuario,ac_estatus,proyecto,idUsuarios');

		$this->db->from('usuarios');

		$this->db->where('usuarios.us_estatus', 1); 

		$this->db->order_by('idUsuarios',"ASC");  



		$this->db->join('actividades', 'usuarios.idUsuarios = actividades.usuarios_idUsuarios', 'INNER');
		$this->db->join('proyectos', 'actividades.proyectos_idProyectos = proyectos.idProyectos', 'INNER');

		$this->db->group_by('actividades.idActividades');  



		$query = $this->db->get();

		//print_r($this->db->last_query());    

		if($query->num_rows() > 0){

			return $query->result();

		} else {

			return null;

		}

	}



	function traer_usuarios_por_proyecto()

	{		 
		$this->db->query("SET lc_time_names = 'es_ES'");
		$this->db->select_sum('horas');

		$this->db->distinct();
		$this->db->select('nombre,observaciones,fecha,fecha_fin,color_usuario,ac_estatus,proyecto');
		$this->db->from('usuarios');
		$this->db->where('usuarios.us_estatus', 1); 
		$this->db->order_by('idUsuarios',"ASC");  

		$this->db->join('actividades', 'usuarios.idUsuarios = actividades.usuarios_idUsuarios', 'INNER');
		$this->db->join('proyectos', 'actividades.proyectos_idProyectos = proyectos.idProyectos', 'INNER');
		$this->db->group_by('idActividades');  
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->result();
		} else {
			return null;
		}
	}




	function traer_actividades_por_proyectos()

	{
		$this->db->select('*');
		$this->db->select_sum('horas');
		$this->db->from('actividades');
		$this->db->join('proyectos', 'actividades.proyectos_idProyectos = proyectos.idProyectos', 'INNER');
		$this->db->join('clientes', 'proyectos.clientes_idClientes = clientes.idClientes', 'INNER'); 
		$this->db->where('proyectos.proy_estatus', 1);  
		$this->db->group_by('proyectos.proyecto');  
		$this->db->order_by('fecha','DESC');  
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->result();
		} else {
			return null;
		}
	}



	function traer_proyectos_por_colaborador()

	{




		$this->db->select('nombre,horas');
				$this->db->distinct();

		$this->db->select_sum('horas');
		$this->db->from('usuarios');

		$this->db->where('usuarios.us_estatus', 1); 

		$this->db->order_by('idUsuarios',"ASC");  



		$this->db->join('actividades', 'usuarios.idUsuarios = actividades.usuarios_idUsuarios', 'INNER');
		$this->db->join('proyectos', 'actividades.proyectos_idProyectos = proyectos.idProyectos', 'INNER');

		$this->db->group_by('nombre');  



		$query = $this->db->get();

		//print_r($this->db->last_query());    

		if($query->num_rows() > 0){

			return $query->result();

		} else {

			return null;

		}


	}



	function traer_usuarios()
	{
		$this->db->select('nombre, color_usuario,idUsuarios');
		$this->db->from('usuarios');
		$this->db->where('usuarios.us_estatus', 1);  
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		} else {
			return null;
		}
	}


}









?>