<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_clientes extends CI_Model{

	var $table = 'clientes';
	var $column_order = array(null, 'cliente','contacto'); 
	var $column_search = array('cliente','contacto'); 
	var $order = array('idClientes' => 'asc');

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('email');
		$this->load->library('session');
		$this->load->helper(array('url'));


	}


	public function traer_datos()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}


	private function _get_datatables_query()
	{


		$this->db->from($this->table);
		$i = 0;

		foreach ($this->column_search as $item)  
		{
			if($_POST['search']['value']) 
			{

				if($i===0) 
				{
					$this->db->group_start();  
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)  
					$this->db->group_end();  
			}
			$i++;
		}

		if(isset($_POST['order'])) 
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}



	public function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id) {
		$this->db->from($this->table);
		$this->db->where('idClientes',$id);
		$query = $this->db->get();

		return $query->row();
	}


	private function _insertar_bitacora($data, $tipo)
	{

		switch ($data['cli_estatus']) {
			case '1':
			$estatus = "Activo";				
			break;
			case '0':
			$estatus = "Inactivo";				
			break;			 
		}		


		$this->db->set('accion', $tipo . ' de Cliente: '. $data['cliente'] . ', contacto: ' . $data['contacto'] . ', estatus: ' . $estatus);
		$this->db->set('usuario', $this->session->userdata("nombre"));
		$this->db->insert('bitacora'); 
	}

	function insertar_cliente($data){
		$this->db->insert($this->table,$data);
		$this->_insertar_bitacora($data, 'Alta');
		return $this->db->insert_id();

	}


	

	public function update($where, $data) {
		$this->db->update($this->table, $data, $where);
		$this->_insertar_bitacora($data, 'Actualización');
 
		return $this->db->affected_rows();
	}

/*
function insertar_usuario($data,$id_codigo, $correo,$codigo)
	{

		$this->db->insert('usuarios',$data);
		$insert_id = $this->db->insert_id();
		$data_estudiantes_por_codigos = array(
			'usuarios_idUsuarios'=>$insert_id,		
			'codigos_idCodigos'=>$id_codigo,		
		);
		$this->db->insert('usuarios_por_codigos',$data_estudiantes_por_codigos);
		$actualizar_usado = array(
			'utilizado' => '1'
		);

		$this->db->where('idCodigos', $id_codigo);
		$this->db->update('codigos', $actualizar_usado);
		$url = base_url();
 				// correo
		$this->email->from('pearsonaccesscode@pearson.com', 'Pearson Orientación Profesional');
		$this->email->to($correo); 

		$this->email->subject('Confirmación de Registro');
		$this->email->message('Muchas gracias por tu registro.<br> El código que utilizaste fue: '.$codigo);
		$this->email->set_mailtype('html');

		

		return ($this->db->affected_rows() != 1) ? false : true;

	}


	function comprobarExistencia($codigo){
		$this->db->select('idCodigos,codigo,utilizado');
		$this->db->from('codigos');
		$this->db->where('codigo',$codigo);
		$this->db->limit(1);
		$this->db->order_by('idCodigos', 'DESC');


		$query = $this->db->get(); 
		foreach ($query->result() as $row)
		{
			$utilizado_temp = $row->utilizado;
		}

		if($utilizado_temp == 1){

			$this->db->select('idCodigos,codigo,utilizado');
			$this->db->from('codigos');
			$this->db->where('codigo',$codigo);
			$this->db->limit(1); 
			$this->db->order_by('idCodigos', 'ASC');
			$query = $this->db->get();					

		}

		if ($query->num_rows() > 0){
			return $query->result();
		}
		else {
			return false;
		}
	}

	function traer_carreras($idUsuarios)
	{
		$this->db->select('*');
		$this->db->from('carreras_resultados');
		$this->db->where('carreras_resultados.usuarios_idUsuarios', $idUsuarios);  
		$this->db->order_by('idCarreras_resultados',"DESC");  
		$this->db->limit(1); 
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->result();
		} else {
			return null;
		}
	}
   

	 */

}




?>