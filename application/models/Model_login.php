<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_login extends CI_Model{
	 
	public function __construct() {
		parent::__construct();
			$this->load->database();
			$this->load->library('encrypt');

	}
   
	public function can_log_in($correo_login, $password_login) {
		 
		$query2 = $this->db->query("SELECT password FROM usuarios WHERE us_estatus = '1' AND correo = '".$correo_login."';");
 			$passwordEncrip =  null;

		if ($query2->num_rows() > 0)
		{
			$row = $query2->row(); 
			$passwordEncrip =  $row->password;
		}
		$passwordDecript = $this->encrypt->decode($passwordEncrip);		
		

		if($password_login == $passwordDecript ){
			$this->db->select('idUsuarios, correo, nombre');
			$this->db->from('usuarios');
			$this->db->where('correo',$correo_login);		 
			$query=$this->db->get();
			$resultado = $query->row();			
			return $resultado;		
		}
		
	}
	public function recuperarPassword($email_recuperar) {
		 
		$query2 = $this->db->query("SELECT correo,password FROM usuarios WHERE us_estatus = '1' AND correo = '".$email_recuperar."';");
 			$passwordEncrip =  null;

		if ($query2->num_rows() > 0)
		{
			$row = $query2->row(); 
			$passwordEncrip =  $row->password;
			
			$passwordDecript = $this->encrypt->decode($passwordEncrip);		
			return $passwordDecript;

		}else {
			return false;
		}
		
		 
				
		
	}
}



?>