-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 26-02-2020 a las 15:27:28
-- Versión del servidor: 5.5.62-cll
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pongavan_control_actividades`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades`
--

CREATE TABLE `actividades` (
  `idActividades` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `horas` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `observaciones` text COLLATE utf8_bin,
  `ac_estatus` int(1) DEFAULT NULL,
  `ac_tp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `proyectos_idProyectos` int(11) NOT NULL,
  `usuarios_idUsuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `actividades`
--

INSERT INTO `actividades` (`idActividades`, `fecha`, `fecha_fin`, `horas`, `observaciones`, `ac_estatus`, `ac_tp`, `proyectos_idProyectos`, `usuarios_idUsuarios`) VALUES
(5, '2020-01-17', '2020-01-18', '2', 'Programar plantilla', 2, '2020-01-13 01:39:53', 1, 2),
(9, '2020-01-16', '2020-01-18', '6', 'Realizar diseño', 2, '2020-01-13 02:59:22', 2, 2),
(10, '2020-01-22', '2020-01-25', '8', 'Crear WordPress', 1, '2020-01-13 03:03:34', 3, 2),
(13, '2020-01-15', '2020-01-16', '2', 'observa3', 1, '2020-01-14 16:48:02', 2, 15),
(40, '2020-01-18', '2020-01-22', '5', 'Descargar archivos', 1, '2020-01-17 17:16:04', 1, 19),
(41, '2020-01-01', '2020-01-01', '2', 'Actividad realizada', 4, '2020-01-17 21:43:45', 4, 2),
(42, '2020-01-17', '2020-01-17', '1', 'Actividad xx', 1, '2020-01-17 21:50:50', 4, 2),
(43, '2020-01-17', '2020-01-17', '1', 'act 2', 1, '2020-01-17 21:51:20', 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `idBitacora` int(11) NOT NULL,
  `accion` text,
  `usuario` varchar(45) DEFAULT NULL,
  `tp_bitacora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bitacora`
--

INSERT INTO `bitacora` (`idBitacora`, `accion`, `usuario`, `tp_bitacora`) VALUES
(23, 'Actualización de Actividad: Realizar render, horas: 2, estatus: Aplazada', 'Jonatan', '2020-01-14 21:53:15'),
(24, 'Alta de Cliente: test 2, contacto: test contacto, estatus: Activo', 'Jonatan', '2020-01-14 21:59:15'),
(25, 'Actualización de Cliente: Test, contacto: jona, estatus: Activo', 'Jonatan', '2020-01-14 21:59:27'),
(26, 'Actualización de Cliente: test 2, contacto: test contacto, estatus: Activo', 'Jonatan', '2020-01-14 22:00:30'),
(27, 'Alta de Proyecto: Nuevo, estatus: Activo', 'Jonatan', '2020-01-14 22:03:06'),
(28, 'Alta de Colaborador: Test, estatus: Activo', 'Jonatan', '2020-01-15 19:55:51'),
(29, 'Actualización de Colaborador: Test, estatus: Activo', 'Jonatan', '2020-01-15 19:56:07'),
(30, 'Actualización de Colaborador: Test, estatus: Inactivo', 'Jonatan', '2020-01-15 19:56:20'),
(31, 'Actualización de Actividad: Crear WordPress, horas: 8, estatus: En proceso', 'Jonatan', '2020-01-17 00:59:11'),
(32, 'Actualización de Actividad: Programar plantilla, horas: 2, estatus: Sin iniciar', 'Jonatan', '2020-01-17 16:21:24'),
(33, 'Actualización de Actividad: Realizar diseño, horas: 6, estatus: Terminado', 'Jonatan', '2020-01-17 16:26:28'),
(34, 'Actualización de Actividad: Crear WordPress, horas: 8, estatus: En proceso', 'Jonatan', '2020-01-17 16:26:36'),
(35, 'Actualización de Actividad: observa3, horas: 2, estatus: En proceso', 'Jonatan', '2020-01-17 16:26:43'),
(36, 'Actualización de Colaborador: Jonatan, estatus: Activo', 'Jonatan', '2020-01-17 17:00:49'),
(37, 'Actualización de Colaborador: Dan, estatus: Activo', 'Jonatan', '2020-01-17 17:01:41'),
(38, 'Actualización de Colaborador: Andi, estatus: Activo', 'Jonatan', '2020-01-17 17:05:32'),
(39, 'Actualización de Colaborador: Aline, estatus: Activo', 'Jonatan', '2020-01-17 17:05:39'),
(40, 'Actualización de Colaborador: Eduardo, estatus: Activo', 'Jonatan', '2020-01-17 17:05:53'),
(41, 'Actualización de Colaborador: Gonzalo, estatus: Activo', 'Jonatan', '2020-01-17 17:05:57'),
(42, 'Actualización de Colaborador: Ilse, estatus: Activo', 'Jonatan', '2020-01-17 17:06:06'),
(43, 'Actualización de Colaborador: Rosa, estatus: Activo', 'Jonatan', '2020-01-17 17:06:11'),
(44, 'Actualización de Colaborador: Test, estatus: Inactivo', 'Jonatan', '2020-01-17 17:06:19'),
(45, 'Actualización de Colaborador: Salomé, estatus: Activo', 'Jonatan', '2020-01-17 17:06:36'),
(46, 'Alta de Colaborador: Joni 2, estatus: Activo', 'Jonatan', '2020-01-17 17:14:48'),
(47, 'Alta de Actividad: Descargar archivos, horas: 5, estatus: En proceso', 'Joni 2', '2020-01-17 17:16:04'),
(48, 'Actualización de Actividad: Descargar archivos, horas: 5, estatus: En proceso', 'Joni 2', '2020-01-17 17:26:06'),
(49, 'Actualización de Actividad: Programar plantilla, horas: 2, estatus: Terminado', 'Jonatan', '2020-01-17 19:29:36'),
(50, 'Actualización de Actividad: Programar plantilla, horas: 2, estatus: Terminado', 'Jonatan', '2020-01-17 19:29:51'),
(51, 'Actualización de Colaborador: Joni 2, estatus: Activo', 'Jonatan', '2020-01-17 20:09:45'),
(52, 'Actualización de Colaborador: Jonatan, estatus: Activo', 'Jonatan', '2020-01-17 20:12:16'),
(53, 'Actualización de Colaborador: Dan, estatus: Activo', 'Jonatan', '2020-01-17 20:12:38'),
(54, 'Actualización de Colaborador: Eduardo, estatus: Activo', 'Jonatan', '2020-01-17 20:13:01'),
(55, 'Actualización de Colaborador: Ilse, estatus: Activo', 'Jonatan', '2020-01-17 20:13:22'),
(56, 'Actualización de Colaborador: Ilse, estatus: Activo', 'Jonatan', '2020-01-17 20:13:41'),
(57, 'Actualización de Colaborador: Joni 2, estatus: Activo', 'Jonatan', '2020-01-17 20:14:03'),
(58, 'Alta de Actividad: Actividad realizada, horas: 2, estatus: Sin iniciar', 'Jonatan', '2020-01-17 21:43:45'),
(59, 'Actualización de Actividad: Actividad realizada, horas: 2, estatus: En proceso', 'Jonatan', '2020-01-17 21:44:09'),
(60, 'Actualización de Actividad: Actividad realizada, horas: 2, estatus: Terminado', 'Jonatan', '2020-01-17 21:44:14'),
(61, 'Actualización de Actividad: Actividad realizada, horas: 2, estatus: Aplazada', 'Jonatan', '2020-01-17 21:44:19'),
(62, 'Alta de Actividad: Actividad xx, horas: 1, estatus: En proceso', 'Jonatan', '2020-01-17 21:50:50'),
(63, 'Alta de Actividad: act 2, horas: 1, estatus: En proceso', 'Jonatan', '2020-01-17 21:51:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `idClientes` int(11) NOT NULL,
  `cliente` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `contacto` text COLLATE utf8_bin,
  `cli_estatus` int(1) NOT NULL DEFAULT '1',
  `cli_tp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`idClientes`, `cliente`, `contacto`, `cli_estatus`, `cli_tp`) VALUES
(1, 'Pearson', 'Persona 1', 1, '2020-01-09 21:36:58'),
(23, 'Cliente 2', 'Persona 2', 1, '2020-01-13 03:02:23'),
(24, 'Bienestar Ahorro', ' Takenobu Yamaguchi', 1, '2020-01-13 18:05:54'),
(25, 'Test', 'jona', 1, '2020-01-14 21:58:52'),
(26, 'test 2', 'test contacto', 1, '2020-01-14 21:59:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE `proyectos` (
  `idProyectos` int(11) NOT NULL,
  `proyecto` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `proy_estatus` int(1) DEFAULT NULL,
  `proy_tp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `clientes_idClientes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`idProyectos`, `proyecto`, `proy_estatus`, `proy_tp`, `clientes_idClientes`) VALUES
(1, 'CSP', 1, '2020-01-13 01:05:15', 1),
(2, 'Proyecto 2', 1, '2020-01-13 02:39:43', 1),
(3, 'Proyecto 3', 1, '2020-01-13 03:03:04', 23),
(4, 'Proyecto XX', 1, '2020-01-14 16:44:07', 23),
(5, 'Nuevo', 1, '2020-01-14 22:03:06', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuarios` int(11) NOT NULL,
  `nombre` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `correo` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `privilegios` int(1) DEFAULT NULL,
  `color_usuario` varchar(20) COLLATE utf8_bin NOT NULL,
  `us_estatus` int(1) DEFAULT NULL,
  `us_tp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuarios`, `nombre`, `correo`, `password`, `privilegios`, `color_usuario`, `us_estatus`, `us_tp`) VALUES
(2, 'Jonatan', 'jonatan@pong.com.mx', 'WlWF7im1TxX347i/1SMhoZENQSfyPmu9cUVZ7RTOiiEEhQEDC3MeLPPjGWZM9KkpO7zPd2f/SyTRIvgoGBEdbA==', 1, 'decd10', 1, '2020-01-09 20:44:35'),
(8, 'Dan', 'dan@pong.com.mx', 'yvjs4dbVJaVQ5z5gZxGzbAmKXNEgkK6cQlOC9pYeeOv9avTpd9dWMlfyxa1l/2eBX2HA0g1Q/xA0trailqxs4Q==', 2, '23e889', 1, '2020-01-13 02:29:12'),
(9, 'Andi', 'andi@pong.com.mx', 'Rf4cstz4W1DrGfEHASS/EG4w+lZ4W3nBFrVzX9Jp6RpneDPRflsSeaT1WPQcrdmiOJ12aUpRfxAZvQhxnhV8Hw==', 1, '1e64d4', 1, '2020-01-13 03:06:28'),
(10, 'Aline', 'aline@pong.com.mx', '6FvjBNdazMvFKFcUwwnfl5+THr4gZGnzvwp3w4I0yJhLxNC3o+f4EROn4mCSpqqb8KYrF6QzNZtyeoeJLawayg==', 1, '871db5', 1, '2020-01-13 03:07:40'),
(11, 'Eduardo', 'eduardo@pong.com.mx', 'WnwPh63vvKK/8cr/ocaXViAD2hAGSOiayCN6fnBBDyb1XUBkD/vZEgibVPaRT1/6Knt95zTYvBLt3LYf6jmMAA==', 2, 'eb0909', 1, '2020-01-13 03:08:52'),
(12, 'Gonzalo', 'gonzalo@pong.com.mx', 'q/waamPkjNmZGh1KX+MvdYPUdsPMjUmhspxemT6uNxXQs8pc8rIsbXD3JwFl2TcQ2WKHjjPAKmLF34hBQzw9gg==', 1, '244d05', 1, '2020-01-13 03:09:13'),
(15, 'Salomé', 'salome@pong.com.mx', 'MHrpY1IId/ekUk3a/LSDfxTzjp39rKwci6Yv5Pf3Oyu4F2/pqhcTxhMr1gUK1GXxRSIrFfDUN4Myn68BaCYQIg==', 1, '0ebff0', 1, '2020-01-13 03:48:01'),
(16, 'Ilse', 'ilse@pong.com.mx', 'X5TihjJNUVqn1fHDo7UoW17G1QlgvprSY2wGfeAyljebBvpvILsoxl7UZw13T/TUAWSrN7nktGxhCsUnw2ED8w==', 2, 'edb649', 1, '2020-01-13 18:03:22'),
(17, 'Rosa', 'rosa@pong.com.mx', 'A79TD1qT9AfrxmLV0fVIuOAHMlI23L1yGjX4CIRVIlItpCO0dZ1zzO0aVgT0EpdfvOKC+h5MQ+WDsTivpsk6BQ==', 1, 'fc00fc', 1, '2020-01-14 17:40:06'),
(18, 'Test', 'test', 'W5M6Kjo0DEE3FcO2sjAdPWeA6LHFzfKlYETWOggx24+fgAF9vYu8O/FS69Xn5S3tDE6XxKKuD9TZp0nI11wIFg==', 1, 'ed9d1d', 0, '2020-01-15 19:55:51'),
(19, 'Joni 2', 'jonatan3@pong.com.mx', 'XMNx6PRVDG3wJNafHXcgIfii6/3m4TVtYufJWpg5WtKo25laytBTK7gJgWtUh6BqGQM1XCRiDANM49R0aua8cQ==', 1, '3b3b3b', 1, '2020-01-17 17:14:48');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD PRIMARY KEY (`idActividades`),
  ADD KEY `fk_actividades_proyectos1_idx` (`proyectos_idProyectos`),
  ADD KEY `fk_actividades_usuarios1_idx` (`usuarios_idUsuarios`);

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`idBitacora`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`idClientes`);

--
-- Indices de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`idProyectos`),
  ADD KEY `fk_proyectos_clientes_idx` (`clientes_idClientes`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuarios`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividades`
--
ALTER TABLE `actividades`
  MODIFY `idActividades` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `idBitacora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `idClientes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `idProyectos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD CONSTRAINT `fk_actividades_proyectos1` FOREIGN KEY (`proyectos_idProyectos`) REFERENCES `proyectos` (`idProyectos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_actividades_usuarios1` FOREIGN KEY (`usuarios_idUsuarios`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD CONSTRAINT `fk_proyectos_clientes` FOREIGN KEY (`clientes_idClientes`) REFERENCES `clientes` (`idClientes`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
